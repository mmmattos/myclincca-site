# SCRIPT PARA LIMPAR UM AGENDAMENTO BY ID.
db.agendaprocedimentos.update(
  {
    _id:  ObjectId("5a60da120c10d716ee273f63")
  },
  {
    $set: {
      pacienteNome:null,
      pacienteTelefone:null,
      pacienteEmail:null,
      pacienteDataNascimento:null,
      honorariosMedicos:null,
      observacao:null,
      user: null,
      procedimento: null,
      disponivel: true
    }
  }
)

# SCRIPT PARA LISTAR UM AGENDAMENTO BY ID.
db.agendaprocedimentos.find(
  {
    _id: ObjectId("5a60da120c10d716ee273f63")
  },
  {
    pacienteNome:1,
    pacienteTelefone:1,
    pacienteEmail:1,
    pacienteDataNascimento:1,
    honorariosMedicos:1,
    observacao:1,
    user:1,
    procedimento:1,
    disponivel:1
  }
)

