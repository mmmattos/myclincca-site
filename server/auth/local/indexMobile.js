'use strict';

import express from 'express';
import passport from 'passport';
import {signToken} from '../auth.service';

var router = express.Router();

router.post('/', function(req, res, next) {
  passport.authenticate('local', function(err, user, info) {
    var error = err || info;
    if(error) {
      return res.status(401).json(error);
    }
    if(!user) {
      return res.status(404).json({message: 'Usuario não encontrado!'});
    }

    if(!user.ativo && user.role != 'admin') {
      return res.status(401).json({message: 'Usuario ainda não autorizado!'});
    }

    var userName = user.name;
    var userEmail = user.email;
    var userId = user._id;
    var token = signToken(user._id, user.role);
    res.json({ token, userId, userName, userEmail });
  })(req, res, next);
});

export default router;
