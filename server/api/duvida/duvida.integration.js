'use strict';

/* globals describe, expect, it, beforeEach, afterEach */

var app = require('../..');
import request from 'supertest';

var newDuvida;

describe('Duvida API:', function() {
  describe('GET /api/duvidas', function() {
    var duvidas;

    beforeEach(function(done) {
      request(app)
        .get('/api/duvidas')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          duvidas = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      expect(duvidas).to.be.instanceOf(Array);
    });
  });

  describe('POST /api/duvidas', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/duvidas')
        .send({
          duvida: 'Nova Duvida',
          emailAutor: 'fulano@exemplo.net',
          ativo: true
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          newDuvida = res.body;
          done();
        });
    });

    it('should respond with the newly created thing', function() {
      expect(newDuvida.duvida).to.equal('Nova Duvida');
      expect(newDuvida.ativo).to.equal(true);
    });
  });

  describe('GET /api/duvidas/:id', function() {
    var duvida;

    beforeEach(function(done) {
      request(app)
        .get(`/api/duvidas/${newDuvida._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          duvida = res.body;
          done();
        });
    });

    afterEach(function() {
      duvida = {};
    });

    it('should respond with the requested thing', function() {
      expect(duvida.duvida).to.equal('Nova Duvida');
      expect(duvida.ativo).to.equal(true);
    });
  });

  describe('PUT /api/duvidas`/:id', function() {
    var updatedDuvida;

    beforeEach(function(done) {
      request(app)
        .put(`/api/duvidas/${newDuvida._id}`)
        .send({
          duvida: 'Duvida Atualizada',
          ativo: true
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          updatedDuvida = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedDuvida = {};
    });

    it('should respond with the updated duvida', function() {
      expect(updatedDuvida.duvida).to.equal('Duvida Atualizada');
      expect(updatedDuvida.ativo).to.equal(true);
    });

    it('should respond with the updated duvida on a subsequent GET', function(done) {
      request(app)
        .get(`/api/duvidas/${newDuvida._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          let duvida = res.body;

          expect(duvida.duvida).to.equal('Duvida Atualizada');
          expect(duvida.ativo).to.equal(true);

          done();
        });
    });
  });

  describe('PATCH /api/duvidas/:id', function() {
    var patchedDuvida;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/duvidas/${newDuvida._id}`)
        .send([
          { op: 'replace', path: '/duvida', value: 'Duvida Atualizada' },
          { op: 'replace', path: '/ativo', value: false }
        ])
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          patchedDuvida = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedDuvida = {};
    });

    it('should respond with the patched duvida', function() {
      expect(patchedDuvida.duvida).to.equal('Duvida atualizada');
      expect(patchedDuvida.ativo).to.equal(false);
    });
  });

  describe('DELETE /api/duvidas/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/duvidas/${newDuvida._id}`)
        .expect(204)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when Duvida does not exist', function(done) {
      request(app)
        .delete(`/api/duvidas/${newDuvida._id}`)
        .expect(404)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });
  });
});
