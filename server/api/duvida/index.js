'use strict';

const express = require('express');
const controller = require('./duvida.controller');
import * as auth from '../../auth/auth.service';
// var bodyParser = require('body-parser');
// var expressJwt = require('express-jwt');
// var jwtauthController = require('../../jwt-auth');

const router = express.Router();

//router.get('/', [bodyParser.urlencoded({extended: true}), jwtauthController], controller.index);
router.get('/', controller.index);
router.get('/:id', controller.show);
router.post('/', controller.create);
router.put('/:id', auth.hasRole('admin'), controller.upsert);
router.patch('/:id', auth.hasRole('admin'), controller.patch);
router.delete('/:id', auth.hasRole('admin'), controller.destroy);

module.exports = router;
