'use strict';

/* globals sinon, describe, expect, it */

var proxyquire = require('proxyquire').noPreserveCache();

var duvidaCtrlStub = {
  index: 'duvidaCtrl.index',
  show: 'duvidaCtrl.show',
  create: 'duvidaCtrl.create',
  upsert: 'duvidaCtrl.upsert',
  patch: 'duvidaCtrl.patch',
  destroy: 'duvidaCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var duvidaIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './duvida.controller': duvidaCtrlStub
});

describe('Duvida API Router:', function() {
  it('should return an express router instance', function() {
    expect(duvidaIndex).to.equal(routerStub);
  });

  describe('GET /api/duvidas', function() {
    it('should route to duvida.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'duvidaCtrl.index')
        ).to.have.been.calledOnce;
    });
  });

  describe('GET /api/duvidas/:id', function() {
    it('should route to duvida.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'duvidaCtrl.show')
        ).to.have.been.calledOnce;
    });
  });

  describe('POST /api/duvidas', function() {
    it('should route to duvida.controller.create', function() {
      expect(routerStub.post
        .withArgs('/', 'duvidaCtrl.create')
        ).to.have.been.calledOnce;
    });
  });

  describe('PUT /api/duvidas/:id', function() {
    it('should route to duvida.controller.upsert', function() {
      expect(routerStub.put
        .withArgs('/:id', 'duvidaCtrl.upsert')
        ).to.have.been.calledOnce;
    });
  });

  describe('PATCH /api/duvidas/:id', function() {
    it('should route to duvida.controller.patch', function() {
      expect(routerStub.patch
        .withArgs('/:id', 'duvidaCtrl.patch')
        ).to.have.been.calledOnce;
    });
  });

  describe('DELETE /api/duvidas/:id', function() {
    it('should route to duvida.controller.destroy', function() {
      expect(routerStub.delete
        .withArgs('/:id', 'duvidaCtrl.destroy')
        ).to.have.been.calledOnce;
    });
  });
});
