'use strict';

import mongoose from 'mongoose';
import {registerEvents} from './duvida.events';

var DuvidaSchema = new mongoose.Schema({
  duvida: {
    type: String,
    required: true,
    unique: false
  },
  emailAutor: {
    type: String,
    lowercase: true,
    required: true
  },
  ativo: {
    type: Boolean,
    required: false,
    default: false
  },
  resposta: {
    type: String,
    require: false,
    unique: false,
  }
},{ timestamps: true });

registerEvents(DuvidaSchema);
export default mongoose.model('Duvida', DuvidaSchema);
