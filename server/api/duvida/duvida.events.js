/**
 * Duvida model events
 */

'use strict';

import {EventEmitter} from 'events';
var DuvidaEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
DuvidaEvents.setMaxListeners(0);

// Model events
var events = {
  save: 'save',
  remove: 'remove'
};

// Register the event emitter to the model events
function registerEvents(Duvida) {
  for(var e in events) {
    let event = events[e];
    Duvida.post(e, emitEvent(event));
  }
}

function emitEvent(event) {
  return function(doc) {
    DuvidaEvents.emit(`${event}:${doc._id}`, doc);
    DuvidaEvents.emit(event, doc);
  };
}

export {registerEvents};
export default DuvidaEvents;
