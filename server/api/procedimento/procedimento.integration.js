'use strict';

/* globals describe, expect, it, beforeEach, afterEach */

var app = require('../..');
import request from 'supertest';

var newProcedimento;

describe('Procedimento API:', function() {
  describe('GET /api/procedimentos', function() {
    var procedimentos;

    beforeEach(function(done) {
      request(app)
        .get('/api/procedimentos')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          procedimentos = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      expect(procedimentos).to.be.instanceOf(Array);
    });
  });

  describe('POST /api/procedimentos', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/procedimentos')
        .send({
          name: 'New Procedimento',
          info: 'This is the brand new procedimento!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          newProcedimento = res.body;
          done();
        });
    });

    it('should respond with the newly created procedimento', function() {
      expect(newProcedimento.name).to.equal('New Procedimento');
      expect(newProcedimento.info).to.equal('This is the brand new procedimento!!!');
    });
  });

  describe('GET /api/procedimentos/:id', function() {
    var procedimento;

    beforeEach(function(done) {
      request(app)
        .get(`/api/procedimentos/${newProcedimento._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          procedimento = res.body;
          done();
        });
    });

    afterEach(function() {
      procedimento = {};
    });

    it('should respond with the requested procedimento', function() {
      expect(procedimento.name).to.equal('New Procedimento');
      expect(procedimento.info).to.equal('This is the brand new procedimento!!!');
    });
  });

  describe('PUT /api/procedimentos/:id', function() {
    var updatedProcedimento;

    beforeEach(function(done) {
      request(app)
        .put(`/api/procedimentos/${newProcedimento._id}`)
        .send({
          name: 'Updated Procedimento',
          info: 'This is the updated procedimento!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          updatedProcedimento = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedProcedimento = {};
    });

    it('should respond with the updated procedimento', function() {
      expect(updatedProcedimento.name).to.equal('Updated Procedimento');
      expect(updatedProcedimento.info).to.equal('This is the updated procedimento!!!');
    });

    it('should respond with the updated procedimento on a subsequent GET', function(done) {
      request(app)
        .get(`/api/procedimentos/${newProcedimento._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          let procedimento = res.body;

          expect(procedimento.name).to.equal('Updated Procedimento');
          expect(procedimento.info).to.equal('This is the updated procedimento!!!');

          done();
        });
    });
  });

  describe('PATCH /api/procedimentos/:id', function() {
    var patchedProcedimento;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/procedimentos/${newProcedimento._id}`)
        .send([
          { op: 'replace', path: '/name', value: 'Patched Procedimento' },
          { op: 'replace', path: '/info', value: 'This is the patched procedimento!!!' }
        ])
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          patchedProcedimento = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedProcedimento = {};
    });

    it('should respond with the patched procedimento', function() {
      expect(patchedProcedimento.name).to.equal('Patched Procedimento');
      expect(patchedProcedimento.info).to.equal('This is the patched procedimento!!!');
    });
  });

  describe('DELETE /api/procedimentos/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/procedimentos/${newProcedimento._id}`)
        .expect(204)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when procedimento does not exist', function(done) {
      request(app)
        .delete(`/api/procedimentos/${newProcedimento._id}`)
        .expect(404)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });
  });
});
