/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/procedimentos              ->  index
 * GET     /api/procedimentos/ativos       ->  getAtivos
 * POST    /api/procedimentos              ->  create
 * GET     /api/procedimentos/:id          ->  show
 * PUT     /api/procedimentos/:id          ->  upsert
 * PATCH   /api/procedimentos/:id          ->  patch
 * DELETE  /api/procedimentos/:id          ->  destroy
 */

'use strict';

import jsonpatch from 'fast-json-patch';
import Procedimento from './procedimento.model';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if(entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function patchUpdates(patches) {
  return function(entity) {
    try {
      jsonpatch.apply(entity, patches, /*validate*/ true);
    } catch(err) {
      return Promise.reject(err);
    }

    return entity.save();
  };
}

function removeEntity(res) {
  return function(entity) {
    if(entity) {
      return entity.remove()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if(!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Procedimentos
export function index(req, res) {
  return Procedimento.find().exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a list of Procedimentos ATIVOS
export function getAtivos(req, res) {
  return Procedimento.find({
    ativo: true
  })
  .sort({procedimento: 1})
  .exec()
  .then(respondWithResult(res))
  .catch(handleError(res));
}

// Gets a single Procedimento from the DB
export function show(req, res) {
  return Procedimento.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Creates a new Procedimento in the DB
export function create(req, res) {
  console.log('REQ BODY: ', req.body);
  return Procedimento.create(req.body)
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
}

// Upserts the given Procedimento in the DB at the specified ID
export function upsert(req, res) {
  if(req.body._id) {
    delete req.body._id;
  }
  return Procedimento.findOneAndUpdate({_id: req.params.id}, req.body, {
    new: true, upsert: true, setDefaultsOnInsert: true, runValidators: true
  }).exec()

    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Updates an existing Procedimento in the DB
export function patch(req, res) {
  if(req.body._id) {
    delete req.body._id;
  }
  return Procedimento.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(patchUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a Procedimento from the DB
export function destroy(req, res) {
  console.log('PARAMS ID: ', req.params.id)
  return Procedimento.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}
