'use strict';

/* globals sinon, describe, expect, it */

var proxyquire = require('proxyquire').noPreserveCache();

var procedimentoCtrlStub = {
  index: 'procedimentoCtrl.index',
  show: 'procedimentoCtrl.show',
  create: 'procedimentoCtrl.create',
  upsert: 'procedimentoCtrl.upsert',
  patch: 'procedimentoCtrl.patch',
  destroy: 'procedimentoCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var procedimentoIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './procedimento.controller': procedimentoCtrlStub
});

describe('Procedimento API Router:', function() {
  it('should return an express router instance', function() {
    expect(procedimentoIndex).to.equal(routerStub);
  });

  describe('GET /api/procedimentos', function() {
    it('should route to procedimento.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'procedimentoCtrl.index')
        ).to.have.been.calledOnce;
    });
  });

  describe('GET /api/procedimentos/:id', function() {
    it('should route to procedimento.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'procedimentoCtrl.show')
        ).to.have.been.calledOnce;
    });
  });

  describe('POST /api/procedimentos', function() {
    it('should route to procedimento.controller.create', function() {
      expect(routerStub.post
        .withArgs('/', 'procedimentoCtrl.create')
        ).to.have.been.calledOnce;
    });
  });

  describe('PUT /api/procedimentos/:id', function() {
    it('should route to procedimento.controller.upsert', function() {
      expect(routerStub.put
        .withArgs('/:id', 'procedimentoCtrl.upsert')
        ).to.have.been.calledOnce;
    });
  });

  describe('PATCH /api/procedimentos/:id', function() {
    it('should route to procedimento.controller.patch', function() {
      expect(routerStub.patch
        .withArgs('/:id', 'procedimentoCtrl.patch')
        ).to.have.been.calledOnce;
    });
  });

  describe('DELETE /api/procedimentos/:id', function() {
    it('should route to procedimento.controller.destroy', function() {
      expect(routerStub.delete
        .withArgs('/:id', 'procedimentoCtrl.destroy')
        ).to.have.been.calledOnce;
    });
  });
});
