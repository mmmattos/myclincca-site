'use strict';

import mongoose from 'mongoose';
import {registerEvents} from './procedimento.events';

var ProcedimentoSchema = new mongoose.Schema({
  procedimento: {
    type: String,
    unique: true,
    required: true
  },
  descricao: {
    type: String,
    unique: true,
    required: false
  },
  duracao: {
    type: Number,
    unique: false,
    required: true
  },
  tipo: {
    type: String,
    unique: false,
    required: true
  },
  detalhes: {
    type: [String],
    unique: false,
    required: true
  },
  valor: {
    type: String,
    require: true,
    unique: false
  },
  ativo: {
    type: Boolean,
    unique: false,
    required: false,
    default: true
  },
  emailAutor: {
    type: String,
    required: true,
    unique: false
  }

},{ timestamps: true });

registerEvents(ProcedimentoSchema);
export default mongoose.model('Procedimento', ProcedimentoSchema);
