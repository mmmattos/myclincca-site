/**
 * Procedimento model events
 */

'use strict';

import {EventEmitter} from 'events';
var ProcedimentoEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
ProcedimentoEvents.setMaxListeners(0);

// Model events
var events = {
  save: 'save',
  remove: 'remove'
};

// Register the event emitter to the model events
function registerEvents(Procedimento) {
  for(var e in events) {
    let event = events[e];
    Procedimento.post(e, emitEvent(event));
  }
}

function emitEvent(event) {
  return function(doc) {
    ProcedimentoEvents.emit(event + ':' + doc._id, doc);
    ProcedimentoEvents.emit(event, doc);
  };
}

export {registerEvents};
export default ProcedimentoEvents;
