/**
 * Clinica model events
 */

'use strict';

import {EventEmitter} from 'events';
var ClinicaEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
ClinicaEvents.setMaxListeners(0);

// Model events
var events = {
  save: 'save',
  remove: 'remove'
};

// Register the event emitter to the model events
function registerEvents(Clinica) {
  for(var e in events) {
    let event = events[e];
    Clinica.post(e, emitEvent(event));
  }
}

function emitEvent(event) {
  return function(doc) {
    ClinicaEvents.emit(event + ':' + doc._id, doc);
    ClinicaEvents.emit(event, doc);
  };
}

export {registerEvents};
export default ClinicaEvents;
