/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/clinicas              ->  index
 * POST    /api/clinicas              ->  create
 * GET     /api/clinicas/:id          ->  show
 * PUT     /api/clinicas/:id          ->  upsert
 * PATCH   /api/clinicas/:id          ->  patch
 * DELETE  /api/clinicas/:id          ->  destroy
 */

'use strict';

import jsonpatch from 'fast-json-patch';
import jwtLib from 'jsonwebtoken';
import Clinica from './clinica.model';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if(entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function patchUpdates(patches) {
  return function(entity) {
    try {
      jsonpatch.apply(entity, patches, /*validate*/ true);
    } catch(err) {
      return Promise.reject(err);
    }

    return entity.save();
  };
}

function removeEntity(res) {
  return function(entity) {
    if(entity) {
      return entity.remove()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if(!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Clinicas
export function index(req, res) {
  return Clinica.find().sort({ordem: 1}).exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a list of Clinicas PROTECTED BY TOKEN!!!
export function indexp(req, res) {
  var token = req.headers['x-access-token'];
  if(typeof token === 'undefined') {
    return res.status(401).send({
      message: 'TOKEN MISSING!'
    });
  }
  const jwt = jwtLib.decode(token);
  console.log('TOKEN EXP!!!:', jwt.exp);
  if(jwt.exp < Date.now() / 1000) {
    return res.status(401).send({
      message: 'TOKEN EXPIRED!'
    });
  }
  return Clinica.find().exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}


// Gets a single Clinica from the DB
export function show(req, res) {
  return Clinica.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Creates a new Clinica in the DB
export function create(req, res) {
  const findQuery =
    Clinica
    .find().sort({ordem: -1})
    .limit(1);
  findQuery
  .exec(function(err, maxResult) {
    if(err) {
      throw err;
    }
    const currentOrdem = maxResult[0].ordem;
    const proxOrdem = currentOrdem + 1;
    req.body.ordem = proxOrdem;
    console.log('BODY::: ', req.body);
    return Clinica.create(req.body)
      .then(respondWithResult(res, 201))
      .catch(handleError(res));
  })
  .catch(handleError(res));
}

// Upserts the given Clinica in the DB at the specified ID
export function upsert(req, res) {
  if(req.body._id) {
    delete req.body._id;
  }
  return Clinica.findOneAndUpdate({_id: req.params.id}, req.body, {new: true, upsert: true, setDefaultsOnInsert: true, runValidators: true}).exec()

    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Updates an existing Clinica in the DB
export function patch(req, res) {
  if(req.body._id) {
    delete req.body._id;
  }
  return Clinica.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(patchUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a Clinica from the DB
export function destroy(req, res) {
  return Clinica.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}
