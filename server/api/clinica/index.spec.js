'use strict';

/* globals sinon, describe, expect, it */

var proxyquire = require('proxyquire').noPreserveCache();

var clinicaCtrlStub = {
  index: 'clinicaCtrl.index',
  show: 'clinicaCtrl.show',
  create: 'clinicaCtrl.create',
  upsert: 'clinicaCtrl.upsert',
  patch: 'clinicaCtrl.patch',
  destroy: 'clinicaCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var clinicaIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './clinica.controller': clinicaCtrlStub
});

describe('Clinica API Router:', function() {
  it('should return an express router instance', function() {
    expect(clinicaIndex).to.equal(routerStub);
  });

  describe('GET /api/clinicas', function() {
    it('should route to clinica.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'clinicaCtrl.index')
        ).to.have.been.calledOnce;
    });
  });

  describe('GET /api/clinicas/:id', function() {
    it('should route to clinica.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'clinicaCtrl.show')
        ).to.have.been.calledOnce;
    });
  });

  describe('POST /api/clinicas', function() {
    it('should route to clinica.controller.create', function() {
      expect(routerStub.post
        .withArgs('/', 'clinicaCtrl.create')
        ).to.have.been.calledOnce;
    });
  });

  describe('PUT /api/clinicas/:id', function() {
    it('should route to clinica.controller.upsert', function() {
      expect(routerStub.put
        .withArgs('/:id', 'clinicaCtrl.upsert')
        ).to.have.been.calledOnce;
    });
  });

  describe('PATCH /api/clinicas/:id', function() {
    it('should route to clinica.controller.patch', function() {
      expect(routerStub.patch
        .withArgs('/:id', 'clinicaCtrl.patch')
        ).to.have.been.calledOnce;
    });
  });

  describe('DELETE /api/clinicas/:id', function() {
    it('should route to clinica.controller.destroy', function() {
      expect(routerStub.delete
        .withArgs('/:id', 'clinicaCtrl.destroy')
        ).to.have.been.calledOnce;
    });
  });
});
