'use strict';

/* globals describe, expect, it, beforeEach, afterEach */

var app = require('../..');
import request from 'supertest';

var newClinica;

describe('Clinica API:', function() {
  describe('GET /api/clinicas', function() {
    var clinicas;

    beforeEach(function(done) {
      request(app)
        .get('/api/clinicas')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          clinicas = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      expect(clinicas).to.be.instanceOf(Array);
    });
  });

  describe('POST /api/clinicas', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/clinicas')
        .send({
          secao: 'New Clinica',
          conteudo: 'This is the brand new clinica!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          newClinica = res.body;
          done();
        });
    });

    it('should respond with the newly created clinica', function() {
      expect(newClinica.sexao).to.equal('New Clinica');
      expect(newClinica.conteudo).to.equal('This is the brand new clinica!!!');
    });
  });

  describe('GET /api/clinicas/:id', function() {
    var clinica;

    beforeEach(function(done) {
      request(app)
        .get(`/api/clinicas/${newClinica._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          clinica = res.body;
          done();
        });
    });

    afterEach(function() {
      clinica = {};
    });

    it('should respond with the requested clinica', function() {
      expect(clinica.secao).to.equal('New Clinica');
      expect(clinica.conteudo).to.equal('This is the brand new clinica!!!');
    });
  });

  describe('PUT /api/clinicas/:id', function() {
    var updatedClinica;

    beforeEach(function(done) {
      request(app)
        .put(`/api/clinicas/${newClinica._id}`)
        .send({
          secao: 'Updated Clinica',
          conteudo: 'This is the updated clinica!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          updatedClinica = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedClinica = {};
    });

    it('should respond with the updated clinica', function() {
      expect(updatedClinica.secao).to.equal('Updated Clinica');
      expect(updatedClinica.conteudo).to.equal('This is the updated clinica!!!');
    });

    it('should respond with the updated clinica on a subsequent GET', function(done) {
      request(app)
        .get(`/api/clinicas/${newClinica._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          let clinica = res.body;

          expect(clinica.secao).to.equal('Updated Clinica');
          expect(clinica.conteudo).to.equal('This is the updated clinica!!!');

          done();
        });
    });
  });

  describe('PATCH /api/clinicas/:id', function() {
    var patchedClinica;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/clinicas/${newClinica._id}`)
        .send([
          { op: 'replace', path: '/secao', value: 'Patched Clinica' },
          { op: 'replace', path: '/conteudo', value: 'This is the patched clinica!!!' }
        ])
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          patchedClinica = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedClinica = {};
    });

    it('should respond with the patched clinica', function() {
      expect(patchedClinica.secao).to.equal('Patched Clinica');
      expect(patchedClinica.conteudo).to.equal('This is the patched clinica!!!');
    });
  });

  describe('DELETE /api/clinicas/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/clinicas/${newClinica._id}`)
        .expect(204)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when clinica does not exist', function(done) {
      request(app)
        .delete(`/api/clinicas/${newClinica._id}`)
        .expect(404)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });
  });
});
