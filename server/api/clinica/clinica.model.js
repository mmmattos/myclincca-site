'use strict';

import mongoose from 'mongoose';
import {registerEvents} from './clinica.events';

var ClinicaSchema = new mongoose.Schema({
  secao: {
    type: String,
    required: false,
    unique: false
  },
  tipo: {
    type: String,
    required: true,
    unique: false
  },
  conteudo: {
    type: String,
    required: true,
    unique: false
  },
  ordem: {
    type: Number,
    required: true,
    unique: true
  },
  emailAutor: {
    type: String,
    required: true,
    unique: false
  },
  ativo: {
    type: Boolean,
    required: true,
    unique: false
  }
},{ timestamps: true });

registerEvents(ClinicaSchema);
export default mongoose.model('Clinica', ClinicaSchema);
