/**
 * Tipoprocedimento model events
 */

'use strict';

import {EventEmitter} from 'events';
var TipoprocedimentoEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
TipoprocedimentoEvents.setMaxListeners(0);

// Model events
var events = {
  save: 'save',
  remove: 'remove'
};

// Register the event emitter to the model events
function registerEvents(Tipoprocedimento) {
  for(var e in events) {
    let event = events[e];
    Tipoprocedimento.post(e, emitEvent(event));
  }
}

function emitEvent(event) {
  return function(doc) {
    TipoprocedimentoEvents.emit(event + ':' + doc._id, doc);
    TipoprocedimentoEvents.emit(event, doc);
  };
}

export {registerEvents};
export default TipoprocedimentoEvents;
