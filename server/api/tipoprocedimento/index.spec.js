'use strict';

/* globals sinon, describe, expect, it */

var proxyquire = require('proxyquire').noPreserveCache();

var tipoprocedimentoCtrlStub = {
  index: 'tipoprocedimentoCtrl.index',
  show: 'tipoprocedimentoCtrl.show',
  create: 'tipoprocedimentoCtrl.create',
  upsert: 'tipoprocedimentoCtrl.upsert',
  patch: 'tipoprocedimentoCtrl.patch',
  destroy: 'tipoprocedimentoCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var tipoprocedimentoIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './tipoprocedimento.controller': tipoprocedimentoCtrlStub
});

describe('Tipoprocedimento API Router:', function() {
  it('should return an express router instance', function() {
    expect(tipoprocedimentoIndex).to.equal(routerStub);
  });

  describe('GET /api/tipoprocedimentos', function() {
    it('should route to tipoprocedimento.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'tipoprocedimentoCtrl.index')
        ).to.have.been.calledOnce;
    });
  });

  describe('GET /api/tipoprocedimentos/:id', function() {
    it('should route to tipoprocedimento.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'tipoprocedimentoCtrl.show')
        ).to.have.been.calledOnce;
    });
  });

  describe('POST /api/tipoprocedimentos', function() {
    it('should route to tipoprocedimento.controller.create', function() {
      expect(routerStub.post
        .withArgs('/', 'tipoprocedimentoCtrl.create')
        ).to.have.been.calledOnce;
    });
  });

  describe('PUT /api/tipoprocedimentos/:id', function() {
    it('should route to tipoprocedimento.controller.upsert', function() {
      expect(routerStub.put
        .withArgs('/:id', 'tipoprocedimentoCtrl.upsert')
        ).to.have.been.calledOnce;
    });
  });

  describe('PATCH /api/tipoprocedimentos/:id', function() {
    it('should route to tipoprocedimento.controller.patch', function() {
      expect(routerStub.patch
        .withArgs('/:id', 'tipoprocedimentoCtrl.patch')
        ).to.have.been.calledOnce;
    });
  });

  describe('DELETE /api/tipoprocedimentos/:id', function() {
    it('should route to tipoprocedimento.controller.destroy', function() {
      expect(routerStub.delete
        .withArgs('/:id', 'tipoprocedimentoCtrl.destroy')
        ).to.have.been.calledOnce;
    });
  });
});
