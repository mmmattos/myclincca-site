'use strict';

const express = require('express');
const controller = require('./tipoprocedimento.controller');

const router = express.Router();

router.get('/', controller.index);
router.get('/dotipo/:tipo', controller.getByTipo);

module.exports = router;
