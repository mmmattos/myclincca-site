'use strict';

import mongoose from 'mongoose';
import {registerEvents} from './tipoprocedimento.events';

var TipoprocedimentoSchema = new mongoose.Schema({
  tipo: {
    type: String,
    required: true,
    unique: true
  },
  descricao: {
    type: String,
    required: true,
    unique: false
  },
  emailAutor: {
    type: String,
    required: true,
    unique: false
  },
  ativo: {
    type: Boolean,
    required: true,
    unique: false
  }
},{ timestamps: true });

registerEvents(TipoprocedimentoSchema);
export default mongoose.model('Tipoprocedimento', TipoprocedimentoSchema);
