'use strict';

/* globals describe, expect, it, beforeEach, afterEach */

var app = require('../..');
import request from 'supertest';

var newTipoprocedimento;

describe('Tipoprocedimento API:', function() {
  describe('GET /api/tipoprocedimentos', function() {
    var tipoprocedimentos;

    beforeEach(function(done) {
      request(app)
        .get('/api/tipoprocedimentos')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          tipoprocedimentos = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      expect(tipoprocedimentos).to.be.instanceOf(Array);
    });
  });

  describe('POST /api/tipoprocedimentos', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/tipoprocedimentos')
        .send({
          tipo: 'Cirurgia',
          descricao: 'Um novo tipo de procedimento!!!',
          emailAutor: 'alguem@exemplo.net',
          ativo: true

        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          newTipoprocedimento = res.body;
          done();
        });
    });

    it('should respond with the newly created tipoprocedimento', function() {
      expect(newTipoprocedimento.tipo).to.equal('Cirurgia');
      expect(newTipoprocedimento.emailAutor).to.equal('alguem@exemplo.net');
    });
  });

  describe('GET /api/tipoprocedimentos/:id', function() {
    var tipoprocedimento;

    beforeEach(function(done) {
      request(app)
        .get(`/api/tipoprocedimentos/${newTipoprocedimento._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          tipoprocedimento = res.body;
          done();
        });
    });

    afterEach(function() {
      tipoprocedimento = {};
    });

    it('should respond with the requested tipoprocedimento', function() {
      expect(tipoprocedimento.tipo).to.equal('Cirurgia');
      expect(tipoprocedimento.emailAutor).to.equal('alguem@exemplo.net');
    });
  });

  describe('PUT /api/tipoprocedimentos/:id', function() {
    var updatedTipoprocedimento;

    beforeEach(function(done) {
      request(app)
        .put(`/api/tipoprocedimentos/${newTipoprocedimento._id}`)
        .send({
          tipo: 'Consulta',
          descricao: 'Consulta b&aacute;sica'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          updatedTipoprocedimento = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedTipoprocedimento = {};
    });

    it('should respond with the updated tipoprocedimento', function() {
      expect(updatedTipoprocedimento.tipo).to.equal('Consulta');
      expect(updatedTipoprocedimento.descricao).to.equal('Consulta b&aacute;sica');
    });

    it('should respond with the updated tipoprocedimento on a subsequent GET', function(done) {
      request(app)
        .get(`/api/tipoprocedimentos/${newTipoprocedimento._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          let tipoprocedimento = res.body;

          expect(tipoprocedimento.tipo).to.equal('Consulta');
          expect(tipoprocedimento.descricao).to.equal('Consulta b&aacute;sica');

          done();
        });
    });
  });

  describe('PATCH /api/tipoprocedimentos/:id', function() {
    var patchedTipoprocedimento;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/tipoprocedimentos/${newTipoprocedimento._id}`)
        .send([
          { op: 'replace', path: '/tipo', value: 'Patched Tipoprocedimento' },
          { op: 'replace', path: '/descricao', value: 'This is the patched tipoprocedimento!!!' }
        ])
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          patchedTipoprocedimento = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedTipoprocedimento = {};
    });

    it('should respond with the patched tipoprocedimento', function() {
      expect(patchedTipoprocedimento.tipo).to.equal('Patched Tipoprocedimento');
      expect(patchedTipoprocedimento.descricao).to.equal('This is the patched tipoprocedimento!!!');
    });
  });

  describe('DELETE /api/tipoprocedimentos/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/tipoprocedimentos/${newTipoprocedimento._id}`)
        .expect(204)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when tipoprocedimento does not exist', function(done) {
      request(app)
        .delete(`/api/tipoprocedimentos/${newTipoprocedimento._id}`)
        .expect(404)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });
  });
});
