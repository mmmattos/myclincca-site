'use strict';

/* globals describe, expect, it, beforeEach, afterEach */

var app = require('../..');
import request from 'supertest';

var newEnlace;

describe('Enlace API:', function() {
  describe('GET /api/enlaces', function() {
    var enlaces;

    beforeEach(function(done) {
      request(app)
        .get('/api/enlaces')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          enlaces = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      expect(enlaces).to.be.instanceOf(Array);
    });
  });

  describe('POST /api/enlaces', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/enlaces')
        .send({
          url: 'http://www.google.com',
          titulo: 'Novo Enlace',
          emailAutor: 'fulano@exemplo.net',
          ativo: true
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          newEnlace = res.body;
          done();
        });
    });

    it('should respond with the newly created thing', function() {
      expect(newEnlace.url).to.Equal('http://www.google.com');
      expect(newEnlace.titulo).to.equal('Novo Enlace');
      expect(newEnlace.ativo).to.equal(true);
    });
  });

  describe('GET /api/enlaces/:id', function() {
    var enlace;

    beforeEach(function(done) {
      request(app)
        .get(`/api/enlaces/${newEnlace._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          enlace = res.body;
          done();
        });
    });

    afterEach(function() {
      enlace = {};
    });

    it('should respond with the requested thing', function() {
      expect(enlace.url).to.equal('http://www.google.com');
      expect(enlace.titulo).to.equal('Novo Enlace');
      expect(enlace.ativo).to.equal(true);
    });
  });

  describe('PUT /api/enlaces`/:id', function() {
    var updatedEnlace;

    beforeEach(function(done) {
      request(app)
        .put(`/api/enlaces/${newEnlace._id}`)
        .send({
          titulo: 'Enlace Atualizado',
          ativo: true
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          updatedEnlace = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedEnlace = {};
    });

    it('should respond with the updated enlace', function() {
      expect(updatedEnlace.url).to.equal('http://www.google.com');
      expect(updatedEnlace.titulo).to.equal('Enlace Atualizado');
      expect(updatedEnlace.ativo).to.equal(true);
    });

    it('should respond with the updated enlace on a subsequent GET', function(done) {
      request(app)
        .get(`/api/enlaces/${newEnlace._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          let enlace = res.body;
          expect(enlace.url).to.equal('http://www.google.com');
          expect(enlace.titulo).to.equal('Enlace Atualizado');
          expect(enlace.ativo).to.equal(true);

          done();
        });
    });
  });

  describe('PATCH /api/enlaces/:id', function() {
    var patchedEnlace;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/enlaces/${newEnlace._id}`)
        .send([
          { op: 'replace', path: '/url', value: 'http://www.bing.com' },
          { op: 'replace', path: '/titulo', value: 'Enlace Atualizado' },
          { op: 'replace', path: '/ativo', value: false }
        ])
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          patchedEnlace = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedEnlace = {};
    });

    it('should respond with the patched enlace', function() {
      expect(patchedEnlace.url).to.equal('http://www.bing.com');
      expect(patchedEnlace.titulo).to.equal('Enlace Atualizado');
      expect(patchedEnlace.ativo).to.equal(false);
    });
  });

  describe('DELETE /api/enlaces/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/enlaces/${newEnlace._id}`)
        .expect(204)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when Enlace does not exist', function(done) {
      request(app)
        .delete(`/api/enlaces/${newEnlace._id}`)
        .expect(404)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });
  });
});
