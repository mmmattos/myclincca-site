Enlace'use strict';

/* globals sinon, describe, expect, it */

var proxyquire = require('proxyquire').noPreserveCache();

var enlaceCtrlStub = {
  index: 'enlaceCtrl.index',
  show: 'enlaceCtrl.show',
  create: 'enlaceCtrl.create',
  upsert: 'enlaceCtrl.upsert',
  patch: 'enlaceCtrl.patch',
  destroy: 'enlaceCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var enlaceIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './enlace.controller': enlaceCtrlStub
});

describe('Enlace API Router:', function() {
  it('should return an express router instance', function() {
    expect(enlaceIndex).to.equal(routerStub);
  });

  describe('GET /api/enlaces', function() {
    it('should route to enlace.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'enlaceCtrl.index')
        ).to.have.been.calledOnce;
    });
  });

  describe('GET /api/enlaces/:id', function() {
    it('should route to enlace.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'enlaceCtrl.show')
        ).to.have.been.calledOnce;
    });
  });

  describe('POST /api/enlaces', function() {
    it('should route to enlace.controller.create', function() {
      expect(routerStub.post
        .withArgs('/', 'enlaceCtrl.create')
        ).to.have.been.calledOnce;
    });
  });

  describe('PUT /api/enlaces/:id', function() {
    it('should route to enlace.controller.upsert', function() {
      expect(routerStub.put
        .withArgs('/:id', 'enlaceCtrl.upsert')
        ).to.have.been.calledOnce;
    });
  });

  describe('PATCH /api/enlaces/:id', function() {
    it('should route to enlace.controller.patch', function() {
      expect(routerStub.patch
        .withArgs('/:id', 'enlaceCtrl.patch')
        ).to.have.been.calledOnce;
    });
  });

  describe('DELETE /api/enlaces/:id', function() {
    it('should route to enlace.controller.destroy', function() {
      expect(routerStub.delete
        .withArgs('/:id', 'enlaceCtrl.destroy')
        ).to.have.been.calledOnce;
    });
  });
});
