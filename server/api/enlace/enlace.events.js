/**
 * Enlace model events
 */

'use strict';

import {EventEmitter} from 'events';
var EnlaceEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
EnlaceEvents.setMaxListeners(0);

// Model events
var events = {
  save: 'save',
  remove: 'remove'
};

// Register the event emitter to the model events
function registerEvents(Enlace) {
  for(var e in events) {
    let event = events[e];
    Enlace.post(e, emitEvent(event));
  }
}

function emitEvent(event) {
  return function(doc) {
    EnlaceEvents.emit(`${event}:${doc._id}`, doc);
    EnlaceEvents.emit(event, doc);
  };
}

export {registerEvents};
export default EnlaceEvents;
