/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/enlaces              ->  index
 * POST    /api/enlaces              ->  create
 * GET     /api/enlaces/:id          ->  show
 * PUT     /api/enlaces/:id          ->  upsert
 * PATCH   /api/enlaces/:id          ->  patch
 * DELETE  /api/enlaces/:id          ->  destroy
 */

'use strict';

import jsonpatch from 'fast-json-patch';
import Enlace from './enlace.model';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if(entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function patchUpdates(patches) {
  return function(entity) {
    try {
      jsonpatch.apply(entity, patches, /*validate*/ true);
    } catch(err) {
      return Promise.reject(err);
    }

    return entity.save();
  };
}

function removeEntity(res) {
  return function(entity) {
    if(entity) {
      return entity.remove()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if(!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Enlaces
export function index(req, res) {
  return Enlace.find().exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a single Enlace from the DB
export function show(req, res) {
  return Enlace.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Creates a new Enlace in the DB
export function create(req, res) {
  return Enlace.create(req.body)
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
}

// Upserts the given Enlace in the DB at the specified ID
export function upsert(req, res) {
  if(req.body._id) {
    delete req.body._id;
  }
  return Enlace.findOneAndUpdate({_id: req.params.id}, req.body, {new: true, upsert: true, setDefaultsOnInsert: true, runValidators: true}).exec()

    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Updates an existing Enlace in the DB
export function patch(req, res) {
  if(req.body._id) {
    console.log('_id: ', req.body._id);
    delete req.body._id;
  }
  console.log('params.id:',req.params);
  return Enlace.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(patchUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a Enlace from the DB
export function destroy(req, res) {
  return Enlace.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}
