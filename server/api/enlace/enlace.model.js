'use strict';

import mongoose from 'mongoose';
import {registerEvents} from './enlace.events';

var EnlaceSchema = new mongoose.Schema({
  url: {
    type: String,
    required: true,
    unique: true
  },
  titulo: {
    type: String,
    required: true,
    unique: true
  },
  emailAutor: {
    type: String,
    lowercase: true,
    required: true
  },
  ativo: {
    type: Boolean,
    required: false,
    default: false
  }
},{ timestamps: true });

registerEvents(EnlaceSchema);
export default mongoose.model('Enlace', EnlaceSchema);
