'use strict';

/* globals sinon, describe, expect, it */

var proxyquire = require('proxyquire').noPreserveCache();

var agendaprocedimentoCtrlStub = {
  index: 'agendaprocedimentoCtrl.index',
  show: 'agendaprocedimentoCtrl.show',
  create: 'agendaprocedimentoCtrl.create',
  upsert: 'agendaprocedimentoCtrl.upsert',
  patch: 'agendaprocedimentoCtrl.patch',
  destroy: 'agendaprocedimentoCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var agendaprocedimentoIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './agendaprocedimento.controller': agendaprocedimentoCtrlStub
});

describe('Agendaprocedimento API Router:', function() {
  it('should return an express router instance', function() {
    expect(agendaprocedimentoIndex).to.equal(routerStub);
  });

  describe('GET /api/agendaprocedimentos', function() {
    it('should route to agendaprocedimento.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'agendaprocedimentoCtrl.index')
        ).to.have.been.calledOnce;
    });
  });

  describe('GET /api/agendaprocedimentos/:id', function() {
    it('should route to agendaprocedimento.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'agendaprocedimentoCtrl.show')
        ).to.have.been.calledOnce;
    });
  });

  describe('POST /api/agendaprocedimentos', function() {
    it('should route to agendaprocedimento.controller.create', function() {
      expect(routerStub.post
        .withArgs('/', 'agendaprocedimentoCtrl.create')
        ).to.have.been.calledOnce;
    });
  });

  describe('PUT /api/agendaprocedimentos/:id', function() {
    it('should route to agendaprocedimento.controller.upsert', function() {
      expect(routerStub.put
        .withArgs('/:id', 'agendaprocedimentoCtrl.upsert')
        ).to.have.been.calledOnce;
    });
  });

  describe('PATCH /api/agendaprocedimentos/:id', function() {
    it('should route to agendaprocedimento.controller.patch', function() {
      expect(routerStub.patch
        .withArgs('/:id', 'agendaprocedimentoCtrl.patch')
        ).to.have.been.calledOnce;
    });
  });

  describe('DELETE /api/agendaprocedimentos/:id', function() {
    it('should route to agendaprocedimento.controller.destroy', function() {
      expect(routerStub.delete
        .withArgs('/:id', 'agendaprocedimentoCtrl.destroy')
        ).to.have.been.calledOnce;
    });
  });
});
