'use strict';

/* globals describe, expect, it, beforeEach, afterEach */

var app = require('../..');
import request from 'supertest';

var newAgendaprocedimento;

describe('Agendaprocedimento API:', function() {
  describe('GET /api/agendaprocedimentos', function() {
    var agendaprocedimentos;

    beforeEach(function(done) {
      request(app)
        .get('/api/agendaprocedimentos')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          agendaprocedimentos = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      expect(agendaprocedimentos).to.be.instanceOf(Array);
    });
  });

  describe('POST /api/agendaprocedimentos', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/agendaprocedimentos')
        .send({
          name: 'New Agendaprocedimento',
          info: 'This is the brand new agendaprocedimento!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          newAgendaprocedimento = res.body;
          done();
        });
    });

    it('should respond with the newly created agendaprocedimento', function() {
      expect(newAgendaprocedimento.name).to.equal('New Agendaprocedimento');
      expect(newAgendaprocedimento.info).to.equal('This is the brand new agendaprocedimento!!!');
    });
  });

  describe('GET /api/agendaprocedimentos/:id', function() {
    var agendaprocedimento;

    beforeEach(function(done) {
      request(app)
        .get(`/api/agendaprocedimentos/${newAgendaprocedimento._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          agendaprocedimento = res.body;
          done();
        });
    });

    afterEach(function() {
      agendaprocedimento = {};
    });

    it('should respond with the requested agendaprocedimento', function() {
      expect(agendaprocedimento.name).to.equal('New Agendaprocedimento');
      expect(agendaprocedimento.info).to.equal('This is the brand new agendaprocedimento!!!');
    });
  });

  describe('PUT /api/agendaprocedimentos/:id', function() {
    var updatedAgendaprocedimento;

    beforeEach(function(done) {
      request(app)
        .put(`/api/agendaprocedimentos/${newAgendaprocedimento._id}`)
        .send({
          name: 'Updated Agendaprocedimento',
          info: 'This is the updated agendaprocedimento!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          updatedAgendaprocedimento = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedAgendaprocedimento = {};
    });

    it('should respond with the updated agendaprocedimento', function() {
      expect(updatedAgendaprocedimento.name).to.equal('Updated Agendaprocedimento');
      expect(updatedAgendaprocedimento.info).to.equal('This is the updated agendaprocedimento!!!');
    });

    it('should respond with the updated agendaprocedimento on a subsequent GET', function(done) {
      request(app)
        .get(`/api/agendaprocedimentos/${newAgendaprocedimento._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          let agendaprocedimento = res.body;

          expect(agendaprocedimento.name).to.equal('Updated Agendaprocedimento');
          expect(agendaprocedimento.info).to.equal('This is the updated agendaprocedimento!!!');

          done();
        });
    });
  });

  describe('PATCH /api/agendaprocedimentos/:id', function() {
    var patchedAgendaprocedimento;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/agendaprocedimentos/${newAgendaprocedimento._id}`)
        .send([
          { op: 'replace', path: '/name', value: 'Patched Agendaprocedimento' },
          { op: 'replace', path: '/info', value: 'This is the patched agendaprocedimento!!!' }
        ])
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          patchedAgendaprocedimento = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedAgendaprocedimento = {};
    });

    it('should respond with the patched agendaprocedimento', function() {
      expect(patchedAgendaprocedimento.name).to.equal('Patched Agendaprocedimento');
      expect(patchedAgendaprocedimento.info).to.equal('This is the patched agendaprocedimento!!!');
    });
  });

  describe('DELETE /api/agendaprocedimentos/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/agendaprocedimentos/${newAgendaprocedimento._id}`)
        .expect(204)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when agendaprocedimento does not exist', function(done) {
      request(app)
        .delete(`/api/agendaprocedimentos/${newAgendaprocedimento._id}`)
        .expect(404)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });
  });
});
