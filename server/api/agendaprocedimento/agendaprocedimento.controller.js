/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/agendaprocedimentos              ->  index
 * POST    /api/agendaprocedimentos              ->  create
 * GET     /api/agendaprocedimentos/:id          ->  show
 * PUT     /api/agendaprocedimentos/:id          ->  upsert
 * PATCH   /api/agendaprocedimentos/:id          ->  patch
 * DELETE  /api/agendaprocedimentos/:id          ->  destroy
 *
 */
'use strict';
const moment = require('moment');
const Promise = require('bluebird');
import jsonpatch from 'fast-json-patch';
import Agendaprocedimento from './agendaprocedimento.model';
import User from '../user/user.model';
import Procedimento from '../procedimento/procedimento.model';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if(entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function patchUpdates(patches) {
  return function(entity) {
    try {
      jsonpatch.apply(entity, patches, /*validate*/ true);
    } catch(err) {
      return Promise.reject(err);
    }
    return entity.save();
  };
}

function removeEntity(res) {
  return function(entity) {
    if(entity) {
      return entity.remove().then(() => {
        res.status(204).end();
      });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if(!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Agendaprocedimentos
export
function index(req, res) {
  return Agendaprocedimento.find()
    .populate('user')
    .populate('procedimento')
    .exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}


// Gets a single Agendaprocedimento from the DB
export
function show(req, res) {
  return Agendaprocedimento.findById(req.params.id).exec().then(handleEntityNotFound(res)).then(respondWithResult(res)).
  catch(handleError(res));
}


// Creates a new Agendaprocedimento in the DB
export
function create(req, res) {
  console.log('CREATE >>> REQ.BODY:', req.body);
  return Agendaprocedimento.create(req.body).then(respondWithResult(res, 201)).
  catch(handleError(res));
}


// Upserts the given Agendaprocedimento in the DB at the specified ID
export
function upsert(req, res) {
  if(req.body._id) {
    delete req.body._id;
  }
  return Agendaprocedimento.findOneAndUpdate({
    _id: req.params.id
  }, req.body, {
    new: true,
    upsert: true,
    setDefaultsOnInsert: true,
    runValidators: true
  }).exec().then(respondWithResult(res)).
  catch(handleError(res));
}


// Updates an existing Agendaprocedimento in the DB
export
function patch(req, res) {
  if(req.body._id) {
    delete req.body._id;
  }
  return Agendaprocedimento.findById(req.params.id).exec().then(handleEntityNotFound(res)).then(patchUpdates(req.body)).then(respondWithResult(res)).
  catch(handleError(res));
}
// Deletes a Agendaprocedimento from the DB
export function destroy(req, res) {
  return Agendaprocedimento.findById(req.params.id).exec().then(handleEntityNotFound(res)).then(removeEntity(res)).
  catch(handleError(res));
}


const timeSlots = [
  '08:00', '08:30', '09:00', '09:30',
  '10:00', '10:30', '11:00', '11:30', '12:00',
  '12:30', '13:00', '13:30', '14:00', '14:30',
  '15:00', '15:30', '16:00', '16:30', '17:00',
  '17:30'
];

export function dodia(req, res) {
  if(!req.params.datap) {
    //console.log('REQ:', req);
    res.status(400).send({
      message: 'Data não informada.'
    });
    return null;
  }
  const datap = moment(req.params.datap).toDate();
  const nextDay = moment(req.params.datap).add(1, 'days').toDate();
  //datap.setHours(0,0,0,0);
  console.log('Data Param:', datap.toString());
  return Agendaprocedimento.find({
    data: {
      $gte: datap,
      $lt: nextDay
    }
  //}).exec().then(handleAgendaDoDia(datap, res)).
  })
  //.populate('user')
  //.populate('procedimento')
  .sort({horario: 1})
  .populate('user')
  .populate('procedimento')
  .exec()
  .then((agenda) => {
    // console.log('EXISTING DATAP AGENDA: ', agenda);
    if(agenda.length > 0) {
      console.log('AGENDA #17: ', agenda[17]);
      return res.status(200).json(agenda);
    }
    console.log('CREATING NEW AGENDA FOR ', datap.toString());
    var promises = [];
    for(var i = 0; i < timeSlots.length; i++) {
            // BELOW JUST FOR TESTING. REMOVE!!!
      const seedNumber = Math.random();
      //const dispBool = seedNumber >= 0.5;
      const dispBool = true;
      console.log('Horario: ', timeSlots[i], ' - Disponivel: ', dispBool);
      var promise = Agendaprocedimento.create({
        data: datap,
        horario: timeSlots[i],
        ativo: true,
        disponivel: dispBool,
        autorizado: true,
        emailAutor: 'myclin.cca@gmail.com',
        procedimento: null,
        user: null
      });
      promises.push(promise);
    }
    Promise.all(promises).then(newAgenda => {
      //console.log('CREATE RESULTS: ', newAgenda);
      return res.status(200).json(newAgenda);
    });
  }).
  catch(handleError(res));
}

export function cancela(req, res) {
  console.log('CANCELA REQ BODY: ', req.body);
  const agendamentoId = req.body.agendamentoId;
  Agendaprocedimento
  .findOne({_id: agendamentoId, disponivel: false}).sort({horario: 1})
  .populate('user')
  .populate('procedimento')
  .exec((err, agendamentoInicial) => {
    if (err || agendamentoInicial === null || agendamentoInicial.procedimento === null) return handleError(err);
    console.log('The Procedimento is: ', agendamentoInicial.procedimento._id);
    console.log('ACHOU AGENDAMENTO PRE-EXISTENTE: ',agendamentoInicial);
    if(agendamentoInicial) {
      console.log('AG: ', agendamentoInicial);
      const dataAg = agendamentoInicial.data;
      // const reqdSlots = req.body.reqdSlots;
      const reqdSlots = agendamentoInicial.procedimento.duracao/30;
      console.log('FOUND REQDSLOTS IN PROC: ', reqdSlots);
      const horarioInicial = agendamentoInicial.horario;
      const horarioFinalIndex = timeSlots.indexOf(horarioInicial)+(reqdSlots-1);
      const horarioFinal = timeSlots[horarioFinalIndex];
      console.log('DATA: ',dataAg, ' - HOR.INIC.: ',horarioInicial,'HOR. FIN.:',horarioFinal,' - REQ.SLOTS: ',reqdSlots);
      if(reqdSlots && reqdSlots > 0) {
        Agendaprocedimento
        .find({ data: dataAg, disponivel: false, horario: { $gte: horarioInicial, $lte: horarioFinal}})
        .limit(reqdSlots)
        .exec((err2, agendAdicionais) => {
          if(err2) {
            console.log('ERROR 500: Erro na leitura dos agendamentos: ', err2);
            return res.status(500).json({
              message: 'Erro na leitura dos agendamentos. Verifique e tente mais tarde.'
            });
          }
          console.log('ACHOU ',agendAdicionais.length);
          let items = [];
          agendAdicionais.forEach((agItem) => {
            agItem.pacienteNome = '';
            agItem.pacienteTelefone = '';
            agItem.pacienteEmail = '';
            agItem.pacienteDataNascimento = '';
            agItem.honorariosMedicos = '';
            agItem.observacao = '';
            agItem.user = null;
            agItem.procedimento = null;
            agItem.disponivel = true;
            agItem
            .save((err3) => {
              if(err3) {
                console.log('ERROR SAVING 500:', err3);
                return res.status(500).json({
                  message: 'Erro ao Cancelar o Agendamento. Verifique sua agenda'
                });
              }
              console.log('AGITEM: ',agItem);
              agItem
                .populate('user')
                .populate('procedimento');
            });
            console.log('SAVE SUCCEEDED FOR ITEM: ', agItem._id);
            items.push(agItem);
          });
          let result = {
            message: 'Agenda Cancelada!',
            agendamentos: items
          };
          return res.status(200).json(result);
        });
      }
    }
  })
  .catch((err) => {
    console.log(err);
  });
}

export function confirma(req, res) {
  console.log('CONFIRMA REQ BODY: ', req.body);
  const agendamentoId = req.body.agendamentoId;
  Agendaprocedimento
  .findOne({_id: agendamentoId, disponivel: true})
  .exec((err, agendamento) => {
    console.log('ACHOU AGENDAMENTO PRE-EXISTENTE: ',agendamento);
    if(agendamento) {
      const dataAg = agendamento.data;
      const reqdSlots = req.body.reqdSlots;
      const horarioInicial = agendamento.horario;
      const horarioFinalIndex = timeSlots.indexOf(horarioInicial)+(reqdSlots-1);
      if (horarioFinalIndex > (timeSlots.length-1)) {
        console.log('ERROR 400 - Nao há horários suficientes!!!');
        return res.status(400).json({
          message: 'Não há horarios disponíveis necessários para este procedimento.'
        });
      }
      const horarioFinal = timeSlots[horarioFinalIndex];
      console.log('DATA: ',dataAg, ' - HOR.INIC.: ',horarioInicial,'HOR. FIN.:',horarioFinal,' - REQ.SLOTS: ',reqdSlots);
      if(reqdSlots && reqdSlots > 0) {
        Agendaprocedimento
        .find({ data: dataAg, disponivel: true, horario: { $gte: horarioInicial, $lte: horarioFinal}})
        .limit(reqdSlots)
        .exec((err2, agendAdicionais) => {
          if(err2) {
            console.log('ERROR 500: Erro na leitura dos agendamentos: ', err2);
            return res.status(500).json({
              message: 'Erro na leitura dos agendamentos. Verifique e tente mais tarde.'
            });
          }
          console.log('ACHOU ',agendAdicionais.length);
          if(agendAdicionais.length < reqdSlots) {
            console.log('ERROR 400 - Nao há horários suficientes!!!');
            return res.status(400).json({
              message: 'Não há horarios disponíveis necessários para este procedimento.'
            });
          }
          let items = [];
          User.findOne({_id: req.body.user})
          .exec((err, medico) => {
            if (err) {
              console.log('Erro:  MEDICO não cadastrado: ', err);
              return res.status(404).json({
                message: 'Erro  MEDICO não cadastrado:'
              });
            }
            console.log('FOUND MEDICO:',medico);
            Procedimento.findOne({_id: req.body.procedimento})
            .exec((err4, procedimento) => {
              if (err4) {
                console.log('Erro: PROCEDIMENTO não cadastrado: ', err4);
                return res.status(404).json({
                  message: 'Erro  PROCEDIMENTO não cadastrado:'
                });
              }
              console.log('FOUND PROC:', procedimento);
              agendAdicionais.forEach((agItem) => {
                agItem.pacienteNome = req.body.pacienteNome;
                agItem.pacienteTelefone = req.body.pacienteTelefone;
                agItem.pacienteEmail = req.body.pacienteEmail;
                agItem.pacienteDataNascimento = req.body.pacienteDataNascimento;
                agItem.honorariosMedicos = req.body.honorariosMedicos;
                agItem.observacao = req.body.observacao;
                agItem.user = medico._id;
                agItem.procedimento = procedimento._id;
                agItem.disponivel = false;
                agItem
                .save((err3) => {
                  if(err3) {
                    console.log('ERROR SAVING 500:', err3);
                    return res.status(500).json({
                      message: 'Erro ao Salvar o Agendamento. Verifique sua agenda'
                    });
                  }
                  console.log('AGITEM: ',agItem);
                  agItem
                    .populate('user')
                    .populate('procedimento');
                });
                console.log('SAVE SUCCEEDED FOR ITEM: ', agItem);
                items.push(agItem);
              });
            })
          });
          let result = {
            message: 'Agenda Confirmada!',
            agendamentos: items
          };
          return res.status(200).json(result);
        });
      }
    }
  });
}
