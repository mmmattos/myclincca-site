'use strict';

import mongoose from 'mongoose';
import {registerEvents} from './agendaprocedimento.events';

var AgendaprocedimentoSchema = new mongoose.Schema({
  data: {
    type: Date,
    unique: false,
    required: true,
    index: true
  },
  horario: {
    type: String,
    unique: false,
    required: true
  },
  emailAutor: {
    type: String,
    required: true,
    unique: false
  },
  ativo: {
    type: Boolean,
    require: true,
    unique: false,
    default: true
  },
  disponivel: {
    type: Boolean,
    required: true,
    unique: false,
    default: false
  },
  autorizado: {
    type: Boolean,
    required: true,
    unique: false,
    default: true
  },
  pacienteNome: {
    type: String,
    required: false,
    unique: false
  },
  pacienteTelefone: {
    type: String,
    required: false,
    unique: false
  },
  pacienteEmail: {
    type: String,
    required: false,
    unique: false
  },
  pacienteDataNascimento: {
    type: String,
    required: false,
    unique: false
  },
  honorariosMedicos: {
    type: String,
    required: false,
    unique: false
  },
  observacao: {
    type: String,
    required: false,
    unique: false
  },
  procedimento: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Procedimento',
    index: true
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    index: true
  }
},{ timestamps: true });

registerEvents(AgendaprocedimentoSchema);
export default mongoose.model('Agendaprocedimento', AgendaprocedimentoSchema);
