/**
 * Agendaprocedimento model events
 */

'use strict';

import {EventEmitter} from 'events';
var AgendaprocedimentoEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
AgendaprocedimentoEvents.setMaxListeners(0);

// Model events
var events = {
  save: 'save',
  remove: 'remove'
};

// Register the event emitter to the model events
function registerEvents(Agendaprocedimento) {
  for(var e in events) {
    let event = events[e];
    Agendaprocedimento.post(e, emitEvent(event));
  }
}

function emitEvent(event) {
  return function(doc) {
    AgendaprocedimentoEvents.emit(event + ':' + doc._id, doc);
    AgendaprocedimentoEvents.emit(event, doc);
  };
}

export {registerEvents};
export default AgendaprocedimentoEvents;
