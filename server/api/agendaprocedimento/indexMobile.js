'use strict';

var express = require('express');
var controller = require('./agendaprocedimento.controller');

var router = express.Router();

router.get('/', controller.index);
router.get('/:id', controller.show);
router.get('/dodia/:datap', controller.dodia);
router.post('/confirma', controller.confirma);
router.post('/cancela', controller.cancela);
router.post('/', controller.create);
router.put('/:id', controller.upsert);
router.patch('/:id', controller.patch);
router.delete('/:id', controller.destroy);

module.exports = router;
