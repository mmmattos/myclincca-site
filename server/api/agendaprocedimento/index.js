'use strict';

var express = require('express');
var controller = require('./agendaprocedimento.controller');
import * as auth from '../../auth/auth.service';

var router = express.Router();

router.get('/', auth.isAuthenticated(), controller.index);
router.get('/:id', controller.show);
router.get('/dodia/:datap', auth.isAuthenticated(), controller.dodia);
router.post('/confirma', auth.isAuthenticated(), controller.confirma);
router.post('/cancela', auth.isAuthenticated(), controller.cancela);
router.post('/', auth.isAuthenticated(), controller.create);
router.put('/:id', auth.isAuthenticated(), controller.upsert);
router.patch('/:id', auth.isAuthenticated(), controller.patch);
router.delete('/:id', controller.destroy);

module.exports = router;
