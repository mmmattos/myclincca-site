/**
 * Agendaconsulta model events
 */

'use strict';

import {EventEmitter} from 'events';
var AgendaconsultaEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
AgendaconsultaEvents.setMaxListeners(0);

// Model events
var events = {
  save: 'save',
  remove: 'remove'
};

// Register the event emitter to the model events
function registerEvents(Agendaconsulta) {
  for(var e in events) {
    let event = events[e];
    Agendaconsulta.post(e, emitEvent(event));
  }
}

function emitEvent(event) {
  return function(doc) {
    AgendaconsultaEvents.emit(event + ':' + doc._id, doc);
    AgendaconsultaEvents.emit(event, doc);
  };
}

export {registerEvents};
export default AgendaconsultaEvents;
