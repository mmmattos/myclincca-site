'use strict';

/* globals sinon, describe, expect, it */

var proxyquire = require('proxyquire').noPreserveCache();

var agendaconsultaCtrlStub = {
  index: 'agendaconsultaCtrl.index',
  show: 'agendaconsultaCtrl.show',
  create: 'agendaconsultaCtrl.create',
  upsert: 'agendaconsultaCtrl.upsert',
  patch: 'agendaconsultaCtrl.patch',
  destroy: 'agendaconsultaCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var agendaconsultaIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './agendaconsulta.controller': agendaconsultaCtrlStub
});

describe('Agendaconsulta API Router:', function() {
  it('should return an express router instance', function() {
    expect(agendaconsultaIndex).to.equal(routerStub);
  });

  describe('GET /api/agendaconsultas', function() {
    it('should route to agendaconsulta.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'agendaconsultaCtrl.index')
        ).to.have.been.calledOnce;
    });
  });

  describe('GET /api/agendaconsultas/:id', function() {
    it('should route to agendaconsulta.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'agendaconsultaCtrl.show')
        ).to.have.been.calledOnce;
    });
  });

  describe('POST /api/agendaconsultas', function() {
    it('should route to agendaconsulta.controller.create', function() {
      expect(routerStub.post
        .withArgs('/', 'agendaconsultaCtrl.create')
        ).to.have.been.calledOnce;
    });
  });

  describe('PUT /api/agendaconsultas/:id', function() {
    it('should route to agendaconsulta.controller.upsert', function() {
      expect(routerStub.put
        .withArgs('/:id', 'agendaconsultaCtrl.upsert')
        ).to.have.been.calledOnce;
    });
  });

  describe('PATCH /api/agendaconsultas/:id', function() {
    it('should route to agendaconsulta.controller.patch', function() {
      expect(routerStub.patch
        .withArgs('/:id', 'agendaconsultaCtrl.patch')
        ).to.have.been.calledOnce;
    });
  });

  describe('DELETE /api/agendaconsultas/:id', function() {
    it('should route to agendaconsulta.controller.destroy', function() {
      expect(routerStub.delete
        .withArgs('/:id', 'agendaconsultaCtrl.destroy')
        ).to.have.been.calledOnce;
    });
  });
});
