'use strict';

/* globals describe, expect, it, beforeEach, afterEach */

var app = require('../..');
import request from 'supertest';

var newAgendaconsulta;

describe('Agendaconsulta API:', function() {
  describe('GET /api/agendaconsultas', function() {
    var agendaconsultas;

    beforeEach(function(done) {
      request(app)
        .get('/api/agendaconsultas')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          agendaconsultas = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      expect(agendaconsultas).to.be.instanceOf(Array);
    });
  });

  describe('POST /api/agendaconsultas', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/agendaconsultas')
        .send({
          name: 'New Agendaconsulta',
          info: 'This is the brand new agendaconsulta!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          newAgendaconsulta = res.body;
          done();
        });
    });

    it('should respond with the newly created agendaconsulta', function() {
      expect(newAgendaconsulta.name).to.equal('New Agendaconsulta');
      expect(newAgendaconsulta.info).to.equal('This is the brand new agendaconsulta!!!');
    });
  });

  describe('GET /api/agendaconsultas/:id', function() {
    var agendaconsulta;

    beforeEach(function(done) {
      request(app)
        .get(`/api/agendaconsultas/${newAgendaconsulta._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          agendaconsulta = res.body;
          done();
        });
    });

    afterEach(function() {
      agendaconsulta = {};
    });

    it('should respond with the requested agendaconsulta', function() {
      expect(agendaconsulta.name).to.equal('New Agendaconsulta');
      expect(agendaconsulta.info).to.equal('This is the brand new agendaconsulta!!!');
    });
  });

  describe('PUT /api/agendaconsultas/:id', function() {
    var updatedAgendaconsulta;

    beforeEach(function(done) {
      request(app)
        .put(`/api/agendaconsultas/${newAgendaconsulta._id}`)
        .send({
          name: 'Updated Agendaconsulta',
          info: 'This is the updated agendaconsulta!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          updatedAgendaconsulta = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedAgendaconsulta = {};
    });

    it('should respond with the updated agendaconsulta', function() {
      expect(updatedAgendaconsulta.name).to.equal('Updated Agendaconsulta');
      expect(updatedAgendaconsulta.info).to.equal('This is the updated agendaconsulta!!!');
    });

    it('should respond with the updated agendaconsulta on a subsequent GET', function(done) {
      request(app)
        .get(`/api/agendaconsultas/${newAgendaconsulta._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          let agendaconsulta = res.body;

          expect(agendaconsulta.name).to.equal('Updated Agendaconsulta');
          expect(agendaconsulta.info).to.equal('This is the updated agendaconsulta!!!');

          done();
        });
    });
  });

  describe('PATCH /api/agendaconsultas/:id', function() {
    var patchedAgendaconsulta;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/agendaconsultas/${newAgendaconsulta._id}`)
        .send([
          { op: 'replace', path: '/name', value: 'Patched Agendaconsulta' },
          { op: 'replace', path: '/info', value: 'This is the patched agendaconsulta!!!' }
        ])
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          patchedAgendaconsulta = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedAgendaconsulta = {};
    });

    it('should respond with the patched agendaconsulta', function() {
      expect(patchedAgendaconsulta.name).to.equal('Patched Agendaconsulta');
      expect(patchedAgendaconsulta.info).to.equal('This is the patched agendaconsulta!!!');
    });
  });

  describe('DELETE /api/agendaconsultas/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/agendaconsultas/${newAgendaconsulta._id}`)
        .expect(204)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when agendaconsulta does not exist', function(done) {
      request(app)
        .delete(`/api/agendaconsultas/${newAgendaconsulta._id}`)
        .expect(404)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });
  });
});
