'use strict';

import mongoose from 'mongoose';
import {registerEvents} from './agendaconsulta.events';

var AgendaconsultaSchema = new mongoose.Schema({
  name: String,
  info: String,
  active: Boolean
});

registerEvents(AgendaconsultaSchema);
export default mongoose.model('Agendaconsulta', AgendaconsultaSchema);
