'use strict';

/* globals sinon, describe, expect, it */

var proxyquire = require('proxyquire').noPreserveCache();

var forgotCtrlStub = {
  index: 'forgotCtrl.index',
  show: 'forgotCtrl.show',
  create: 'forgotCtrl.create',
  upsert: 'forgotCtrl.upsert',
  patch: 'forgotCtrl.patch',
  destroy: 'forgotCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var forgotIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './forgot.controller': forgotCtrlStub
});

describe('Forgot API Router:', function() {
  it('should return an express router instance', function() {
    expect(forgotIndex).to.equal(routerStub);
  });

  describe('GET /api/forgots', function() {
    it('should route to forgot.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'forgotCtrl.index')
        ).to.have.been.calledOnce;
    });
  });

  describe('GET /api/forgots/:id', function() {
    it('should route to forgot.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'forgotCtrl.show')
        ).to.have.been.calledOnce;
    });
  });

  describe('POST /api/forgots', function() {
    it('should route to forgot.controller.create', function() {
      expect(routerStub.post
        .withArgs('/', 'forgotCtrl.create')
        ).to.have.been.calledOnce;
    });
  });

  describe('PUT /api/forgots/:id', function() {
    it('should route to forgot.controller.upsert', function() {
      expect(routerStub.put
        .withArgs('/:id', 'forgotCtrl.upsert')
        ).to.have.been.calledOnce;
    });
  });

  describe('PATCH /api/forgots/:id', function() {
    it('should route to forgot.controller.patch', function() {
      expect(routerStub.patch
        .withArgs('/:id', 'forgotCtrl.patch')
        ).to.have.been.calledOnce;
    });
  });

  describe('DELETE /api/forgots/:id', function() {
    it('should route to forgot.controller.destroy', function() {
      expect(routerStub.delete
        .withArgs('/:id', 'forgotCtrl.destroy')
        ).to.have.been.calledOnce;
    });
  });
});
