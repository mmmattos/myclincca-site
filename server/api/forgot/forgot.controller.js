/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/forgots              ->  index
 * POST    /api/forgots              ->  create
 * GET     /api/forgots/:id          ->  show
 * PUT     /api/forgots/:id          ->  upsert
 * PATCH   /api/forgots/:id          ->  patch
 * DELETE  /api/forgots/:id          ->  destroy
 */

'use strict';
const uuidV4 = require('uuid/v4');
const email = require('mailer');
import jsonpatch from 'fast-json-patch';
import Forgot from './forgot.model';
import User from '../user/user.model';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if(entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function patchUpdates(patches) {
  return function(entity) {
    try {
      jsonpatch.apply(entity, patches, /*validate*/ true);
    } catch(err) {
      return Promise.reject(err);
    }

    return entity.save();
  };
}

function removeEntity(res) {
  return function(entity) {
    if(entity) {
      return entity.remove()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if(!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Forgots
export function index(req, res) {
  return Forgot.find().exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a single Forgot from the DB
export function show(req, res) {
  return Forgot.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Creates a new Forgot in the DB
export function create(req, res) {
  const pwd = process.cwd();
  User.findOne({email: req.body.email}, function(err, user) {
    if (!user) {
      res.send(404);
    }
    const newPass = uuidV4().slice(0,8);
    user.password = newPass;
    return user.save()
    .then(() => {
      email.send({
        host : "smtp.yandex.com",
        port : "465",
        ssl : true,
        domain : "localhost",
        to : "mmmattos@gmail.com",
        from : "myclincca.mailer@yandex.com",
        subject : "Redefinição de Senha",
        template : pwd + "/server/api/forgot/forgot.email.tpl.html",
        data : {
          "usuario" : user.email,
          "senha_temporaria" : newPass
        },
        authentication : "login",        // auth login is supported; anything else $
        username : 'myclincca.mailer@yandex.com',
        password : 'm3d1c1n4'
      },
      function(err, result){
          if(err){ 
            console.log(err); 
            handleError(res); 
            return;
          }
          console.log('looks good');
          res.status(200).send();
          return;
      });
    })
    .catch((err) => {
      console.log('ERROR: ', err);
      handleError(res);
      return;
    });
  });

  //return Forgot.create(req.body)
  //  .then(respondWithResult(res, 201))
  //  .catch(handleError(res));
}

// Upserts the given Forgot in the DB at the specified ID
export function upsert(req, res) {
  if(req.body._id) {
    delete req.body._id;
  }
  return Forgot.findOneAndUpdate({_id: req.params.id}, req.body, {new: true, upsert: true, setDefaultsOnInsert: true, runValidators: true}).exec()

    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Updates an existing Forgot in the DB
export function patch(req, res) {
  if(req.body._id) {
    delete req.body._id;
  }
  return Forgot.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(patchUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a Forgot from the DB
export function destroy(req, res) {
  return Forgot.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}
