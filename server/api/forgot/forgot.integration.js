'use strict';

/* globals describe, expect, it, beforeEach, afterEach */

var app = require('../..');
import request from 'supertest';

var newForgot;

describe('Forgot API:', function() {
  describe('GET /api/forgots', function() {
    var forgots;

    beforeEach(function(done) {
      request(app)
        .get('/api/forgots')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          forgots = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      expect(forgots).to.be.instanceOf(Array);
    });
  });

  describe('POST /api/forgots', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/forgots')
        .send({
          email: 'dummy@x.com',
          genSedd: 'sottam9116'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          newForgot = res.body;
          done();
        });
    });

    it('should respond with the newly created forgot', function() {
      expect(newForgot.email).to.equal('dummy@x.com');
      expect(newForgot.genSeed).to.equal('sottam9116');
    });
  });

  describe('GET /api/forgots/:id', function() {
    var forgot;

    beforeEach(function(done) {
      request(app)
        .get(`/api/forgots/${newForgot._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          forgot = res.body;
          done();
        });
    });

    afterEach(function() {
      forgot = {};
    });

    it('should respond with the requested forgot', function() {
      expect(forgot.email).to.equal('dummy@x.com');
      expect(forgot.genSeed).to.equal('sottam9116');
    });
  });

  describe('PUT /api/forgots/:id', function() {
    var updatedForgot;

    beforeEach(function(done) {
      request(app)
        .put(`/api/forgots/${newForgot._id}`)
        .send({
          email: 'dummy@y.com',
          genSeed: 'yllen2017'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          updatedForgot = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedForgot = {};
    });

    it('should respond with the updated forgot', function() {
      expect(updatedForgot.email).to.equal('dummy@y.com');
      expect(updatedForgot.genSeed).to.equal('yllen2017');
    });

    it('should respond with the updated forgot on a subsequent GET', function(done) {
      request(app)
        .get(`/api/forgots/${newForgot._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          let forgot = res.body;

          expect(forgot.email).to.equal('dummy@y.com');
          expect(forgot.genSeed).to.equal('yllen2017');

          done();
        });
    });
  });

  describe('PATCH /api/forgots/:id', function() {
    var patchedForgot;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/forgots/${newForgot._id}`)
        .send([
          { op: 'replace', path: '/email', value: 'dummy@z.com' },
          { op: 'replace', path: '/genSeed', value: '1234' }
        ])
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          patchedForgot = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedForgot = {};
    });

    it('should respond with the patched forgot', function() {
      expect(patchedForgot.email).to.equal('dummy@z.com');
      expect(patchedForgot.genSeed).to.equal('1234');
    });
  });

  describe('DELETE /api/forgots/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/forgots/${newForgot._id}`)
        .expect(204)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when forgot does not exist', function(done) {
      request(app)
        .delete(`/api/forgots/${newForgot._id}`)
        .expect(404)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });
  });
});
