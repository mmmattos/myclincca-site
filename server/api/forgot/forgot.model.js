'use strict';

import mongoose from 'mongoose';
import {registerEvents} from './forgot.events';

var ForgotSchema = new mongoose.Schema({
  email: String,
  dateReq: Date,
  genSeed: String
});

registerEvents(ForgotSchema);
export default mongoose.model('Forgot', ForgotSchema);
