'use strict';

/* globals sinon, describe, expect, it */

var proxyquire = require('proxyquire').noPreserveCache();

var medicoCtrlStub = {
  index: 'medicoCtrl.index',
  show: 'medicoCtrl.show',
  create: 'medicoCtrl.create',
  upsert: 'medicoCtrl.upsert',
  patch: 'medicoCtrl.patch',
  destroy: 'medicoCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var medicoIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './medico.controller': medicoCtrlStub
});

describe('Medico API Router:', function() {
  it('should return an express router instance', function() {
    expect(medicoIndex).to.equal(routerStub);
  });

  describe('GET /api/medicos', function() {
    it('should route to medico.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'medicoCtrl.index')
        ).to.have.been.calledOnce;
    });
  });

  describe('GET /api/medicos/:id', function() {
    it('should route to medico.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'medicoCtrl.show')
        ).to.have.been.calledOnce;
    });
  });

  describe('POST /api/medicos', function() {
    it('should route to medico.controller.create', function() {
      expect(routerStub.post
        .withArgs('/', 'medicoCtrl.create')
        ).to.have.been.calledOnce;
    });
  });

  describe('PUT /api/medicos/:id', function() {
    it('should route to medico.controller.upsert', function() {
      expect(routerStub.put
        .withArgs('/:id', 'medicoCtrl.upsert')
        ).to.have.been.calledOnce;
    });
  });

  describe('PATCH /api/medicos/:id', function() {
    it('should route to medico.controller.patch', function() {
      expect(routerStub.patch
        .withArgs('/:id', 'medicoCtrl.patch')
        ).to.have.been.calledOnce;
    });
  });

  describe('DELETE /api/medicos/:id', function() {
    it('should route to medico.controller.destroy', function() {
      expect(routerStub.delete
        .withArgs('/:id', 'medicoCtrl.destroy')
        ).to.have.been.calledOnce;
    });
  });
});
