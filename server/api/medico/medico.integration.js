'use strict';

/* globals describe, expect, it, beforeEach, afterEach */

var app = require('../..');
import request from 'supertest';

var newMedico;

describe('Medico API:', function() {
  describe('GET /api/medicos', function() {
    var medicos;

    beforeEach(function(done) {
      request(app)
        .get('/api/medicos')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          medicos = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      expect(medicos).to.be.instanceOf(Array);
    });
  });

  describe('POST /api/medicos', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/medicos')
        .send({
          name: 'New Medico',
          info: 'This is the brand new medico!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          newMedico = res.body;
          done();
        });
    });

    it('should respond with the newly created medico', function() {
      expect(newMedico.name).to.equal('New Medico');
      expect(newMedico.info).to.equal('This is the brand new medico!!!');
    });
  });

  describe('GET /api/medicos/:id', function() {
    var medico;

    beforeEach(function(done) {
      request(app)
        .get(`/api/medicos/${newMedico._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          medico = res.body;
          done();
        });
    });

    afterEach(function() {
      medico = {};
    });

    it('should respond with the requested medico', function() {
      expect(medico.name).to.equal('New Medico');
      expect(medico.info).to.equal('This is the brand new medico!!!');
    });
  });

  describe('PUT /api/medicos/:id', function() {
    var updatedMedico;

    beforeEach(function(done) {
      request(app)
        .put(`/api/medicos/${newMedico._id}`)
        .send({
          name: 'Updated Medico',
          info: 'This is the updated medico!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          updatedMedico = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedMedico = {};
    });

    it('should respond with the updated medico', function() {
      expect(updatedMedico.name).to.equal('Updated Medico');
      expect(updatedMedico.info).to.equal('This is the updated medico!!!');
    });

    it('should respond with the updated medico on a subsequent GET', function(done) {
      request(app)
        .get(`/api/medicos/${newMedico._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          let medico = res.body;

          expect(medico.name).to.equal('Updated Medico');
          expect(medico.info).to.equal('This is the updated medico!!!');

          done();
        });
    });
  });

  describe('PATCH /api/medicos/:id', function() {
    var patchedMedico;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/medicos/${newMedico._id}`)
        .send([
          { op: 'replace', path: '/name', value: 'Patched Medico' },
          { op: 'replace', path: '/info', value: 'This is the patched medico!!!' }
        ])
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          patchedMedico = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedMedico = {};
    });

    it('should respond with the patched medico', function() {
      expect(patchedMedico.name).to.equal('Patched Medico');
      expect(patchedMedico.info).to.equal('This is the patched medico!!!');
    });
  });

  describe('DELETE /api/medicos/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/medicos/${newMedico._id}`)
        .expect(204)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when medico does not exist', function(done) {
      request(app)
        .delete(`/api/medicos/${newMedico._id}`)
        .expect(404)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });
  });
});
