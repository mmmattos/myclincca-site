/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/medicos              ->  index
 * POST    /api/medicos              ->  create
 * GET     /api/medicos/:id          ->  show
 * PUT     /api/medicos/:id          ->  upsert
 * PATCH   /api/medicos/:id          ->  patch
 * DELETE  /api/medicos/:id          ->  destroy
 */

'use strict';

import jsonpatch from 'fast-json-patch';
import jwtLib from 'jsonwebtoken';
import User from '../user/user.model';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if(entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function patchUpdates(patches) {
  return function(entity) {
    try {
      // eslint-disable-next-line prefer-reflect
      jsonpatch.apply(entity, patches, /*validate*/ true);
    } catch(err) {
      return Promise.reject(err);
    }

    return entity.save();
  };
}

function removeEntity(res) {
  return function(entity) {
    if(entity) {
      return entity.remove()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if(!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Medicos
export function index(req, res) {
  return User.find({
    role: 'user',
    ativo: true
  })
  .sort({name: 1})
  .exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a list of Users PROTECTED BY TOKEN!!!
export function indexp(req, res) {
  var token = req.headers['x-access-token'];
  if(typeof token === 'undefined') {
    return res.status(401).send({
      message: 'TOKEN MISSING!'
    });
  }
  const jwt = jwtLib.decode(token);
  console.log('TOKEN EXPIRATION DATE:', jwt.exp);
  if(jwt.exp < Date.now() / 1000) {
    return res.status(401).send({
      message: 'TOKEN EXPIRED!!!'
    });
  }
  return User.find({
    role: 'user',
    ativo: true
  })
  .exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}


