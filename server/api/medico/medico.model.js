'use strict';

import mongoose from 'mongoose';
import {registerEvents} from './medico.events';

var MedicoSchema = new mongoose.Schema({
  name: String,
  info: String,
  active: Boolean
});

registerEvents(MedicoSchema);
export default mongoose.model('Medico', MedicoSchema);
