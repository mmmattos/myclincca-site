/**
 * Ping model events
 */

'use strict';

import {EventEmitter} from 'events';
var PingEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
PingEvents.setMaxListeners(0);

// Model events
var events = {
  save: 'save',
  remove: 'remove'
};

// Register the event emitter to the model events
function registerEvents(Ping) {
  for(var e in events) {
    let event = events[e];
    Ping.post(e, emitEvent(event));
  }
}

function emitEvent(event) {
  return function(doc) {
    PingEvents.emit(event + ':' + doc._id, doc);
    PingEvents.emit(event, doc);
  };
}

export {registerEvents};
export default PingEvents;
