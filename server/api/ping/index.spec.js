'use strict';

/* globals sinon, describe, expect, it */

var proxyquire = require('proxyquire').noPreserveCache();

var pingCtrlStub = {
  index: 'pingCtrl.index',
  show: 'pingCtrl.show',
  create: 'pingCtrl.create',
  upsert: 'pingCtrl.upsert',
  patch: 'pingCtrl.patch',
  destroy: 'pingCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var pingIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './ping.controller': pingCtrlStub
});

describe('Ping API Router:', function() {
  it('should return an express router instance', function() {
    expect(pingIndex).to.equal(routerStub);
  });

  describe('GET /api/pings', function() {
    it('should route to ping.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'pingCtrl.index')
        ).to.have.been.calledOnce;
    });
  });

});
