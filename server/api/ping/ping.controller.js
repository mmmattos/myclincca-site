/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/pings              ->  index
 */

'use strict';

import jsonpatch from 'fast-json-patch';
import Ping from './ping.model';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if(entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Pings
export function index(req, res) {
  res.status(200).send({
    message: 'Ping Ok!'
  });
}
