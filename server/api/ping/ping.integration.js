'use strict';

/* globals describe, expect, it, beforeEach, afterEach */

var app = require('../..');
import request from 'supertest';

var newPing;

describe('Ping API:', function() {
  describe('GET /api/pings', function() {
    var pings;

    beforeEach(function(done) {
      request(app)
        .get('/api/pings')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          pings = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      expect(pings).to.be.instanceOf(Array);
    });
  });

  describe('POST /api/pings', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/pings')
        .send({
          name: 'New Ping',
          info: 'This is the brand new ping!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          newPing = res.body;
          done();
        });
    });

    it('should respond with the newly created ping', function() {
      expect(newPing.name).to.equal('New Ping');
      expect(newPing.info).to.equal('This is the brand new ping!!!');
    });
  });

  describe('GET /api/pings/:id', function() {
    var ping;

    beforeEach(function(done) {
      request(app)
        .get(`/api/pings/${newPing._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          ping = res.body;
          done();
        });
    });

    afterEach(function() {
      ping = {};
    });

    it('should respond with the requested ping', function() {
      expect(ping.name).to.equal('New Ping');
      expect(ping.info).to.equal('This is the brand new ping!!!');
    });
  });

  describe('PUT /api/pings/:id', function() {
    var updatedPing;

    beforeEach(function(done) {
      request(app)
        .put(`/api/pings/${newPing._id}`)
        .send({
          name: 'Updated Ping',
          info: 'This is the updated ping!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          updatedPing = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedPing = {};
    });

    it('should respond with the updated ping', function() {
      expect(updatedPing.name).to.equal('Updated Ping');
      expect(updatedPing.info).to.equal('This is the updated ping!!!');
    });

    it('should respond with the updated ping on a subsequent GET', function(done) {
      request(app)
        .get(`/api/pings/${newPing._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          let ping = res.body;

          expect(ping.name).to.equal('Updated Ping');
          expect(ping.info).to.equal('This is the updated ping!!!');

          done();
        });
    });
  });

  describe('PATCH /api/pings/:id', function() {
    var patchedPing;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/pings/${newPing._id}`)
        .send([
          { op: 'replace', path: '/name', value: 'Patched Ping' },
          { op: 'replace', path: '/info', value: 'This is the patched ping!!!' }
        ])
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          patchedPing = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedPing = {};
    });

    it('should respond with the patched ping', function() {
      expect(patchedPing.name).to.equal('Patched Ping');
      expect(patchedPing.info).to.equal('This is the patched ping!!!');
    });
  });

  describe('DELETE /api/pings/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/pings/${newPing._id}`)
        .expect(204)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when ping does not exist', function(done) {
      request(app)
        .delete(`/api/pings/${newPing._id}`)
        .expect(404)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });
  });
});
