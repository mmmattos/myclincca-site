'use strict';

import mongoose from 'mongoose';
import {registerEvents} from './ping.events';

var PingSchema = new mongoose.Schema({
  name: String,
  info: String,
  active: Boolean
});

registerEvents(PingSchema);
export default mongoose.model('Ping', PingSchema);
