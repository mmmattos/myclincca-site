'use strict';

import User from './user.model';
import config from '../../config/environment';
import jwt from 'jsonwebtoken';

function validationError(res, statusCode) {
  statusCode = statusCode || 422;
  return function(err) {
    return res.status(statusCode).json(err);
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    return res.status(statusCode).send(err);
  };
}

/**
* Get list of users
* restriction: 'admin'
*/
export function index(req, res) {
  return User.find({}, '-salt -password')
  .sort({createdAt: -1})
  .exec()
  .then(users => {
    res.status(200).json(users);
  })
  .catch(handleError(res));
}


export function search(req, res) {
  let q = false;
  let query = {};
  if(req.params.pendentesAtivacao && req.params.pendentesAtivacao !== 'undefined') {
    console.log('GOT Q: ', q);
    q = req.params.pendentesAtivacao;
    if(q) {
      query = {ativo: !q};
    }
  }
  console.log('RESULTING QUERY: ', query);
  if (req.params.searchTerm && req.params.searchTerm !== 'undefined') {;
    console.log('SEARCH TERM:', req.params.searchTerm);
    const re = new RegExp(req.params.searchTerm, 'i');
    return User
      .find(query)
      .or([
        {name: {$regex: re}},
        {email: {$regex: re}},
        {especialidade: {$regex: re}},
        {cremers: {$regex: re}}
      ])
      .sort({createdAt: -1})
      .exec()
      .then(users => {
        res.status(200).json(users);
      })
      .catch(handleError(res));
  }
  console.log('3');
  console.log('NO SEARCH TERM PROVIDED, QUERY BY ACTIVE OR ALL!');
  return User
    .find(query)
    .sort({createdAt: -1})
    .exec()
    .then(users => {
      res.status(200).json(users);
    })
    .catch(handleError(res));
}


/**
* Creates a new user
*/
export function create(req, res) {
  console.log('AT SERVER! BODY IS:::', req.body);
  var newUser = new User(req.body);
  newUser.provider = 'local';
  newUser.role = 'user';
  newUser.save()
  .then(function(user) {
    var token = jwt.sign({ _id: user._id }, config.secrets.session, {
      expiresIn: 60 * 60 * 5
    });
    res.json({ token });
  })
  .catch(validationError(res));
}

/**
* Get a single user
*/
export function show(req, res, next) {
  var userId = req.params.id;

  return User.findById(userId).exec()
  .then(user => {
    if(!user) {
      return res.status(404).end();
    }
    res.json(user.profile);
  })
  .catch(err => next(err));
}

/**
* Deletes a user
* restriction: 'admin'
*/
export function destroy(req, res) {
  console.log('GOT INTO DESTROY METHOD!');
  return User.findByIdAndRemove(req.params.id).exec()
  .then(function() {
    res.status(204).end();
  })
  .catch(handleError(res));
}


export function save(req, res) {
  var userId = req.body._id;
  return User.findById(userId).exec()
  .then(user => {
    user.ativo = !user.ativo;
    console.log('USER PRE SAVE:', user);
    return user.save()
      .then(() => {
        res.status(204).end();
      })
      .catch(validationError(res));
  });
}

/**
* Change a users password
*/
export function changePassword(req, res) {
  var userId = req.user._id;
  var oldPass = String(req.body.oldPassword);
  var newPass = String(req.body.newPassword);

  return User.findById(userId).exec()
  .then(user => {
    if(user.authenticate(oldPass)) {
      user.password = newPass;
      return user.save()
      .then(() => {
        res.status(204).end();
      })
      .catch(validationError(res));
    } else {
      return res.status(403).end();
    }
  });
}

/**
* Toggle Active...
*/
export function toggleActive(req, res) {
  var userId = req.params.id;
  return User.findById(userId).exec()
  .then(user => {
    user.ativo = !user.ativo;
    return user.save()
    .then(() => {
      return res.status(204).end();
    })
    .catch(validationError(res));
  });
}

/**
* Get my info
*/
export function me(req, res, next) {
  var userId = req.user._id;

  return User.findOne({ _id: userId }, '-salt -password').exec()
  .then(user => { // don't ever give out the password or salt
    if(!user) {
      return res.status(401).end();
    }
    res.json(user);
  })
.catch(err => next(err));
}

/**
* Authentication callback
*/
export function authCallback(req, res) {
  res.redirect('/');
}
