/**
 * Depoimento model events
 */

'use strict';

import {EventEmitter} from 'events';
var DepoimentoEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
DepoimentoEvents.setMaxListeners(0);

// Model events
var events = {
  save: 'save',
  remove: 'remove'
};

// Register the event emitter to the model events
function registerEvents(Depoimento) {
  for(var e in events) {
    let event = events[e];
    Depoimento.post(e, emitEvent(event));
  }
}

function emitEvent(event) {
  return function(doc) {
    DepoimentoEvents.emit(`${event}:${doc._id}`, doc);
    DepoimentoEvents.emit(event, doc);
  };
}

export {registerEvents};
export default DepoimentoEvents;
