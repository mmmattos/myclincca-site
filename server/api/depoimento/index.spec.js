'use strict';

/* globals sinon, describe, expect, it */

var proxyquire = require('proxyquire').noPreserveCache();

var depoimentoCtrlStub = {
  index: 'depoimentoCtrl.index',
  show: 'depoimentoCtrl.show',
  create: 'depoimentoCtrl.create',
  upsert: 'depoimentoCtrl.upsert',
  patch: 'depoimentoCtrl.patch',
  destroy: 'depoimentoCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var depoimentoIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './depoimento.controller': depoimentoCtrlStub
});

describe('Depoimento API Router:', function() {
  it('should return an express router instance', function() {
    expect(depoimentoIndex).to.equal(routerStub);
  });

  describe('GET /api/depoimentos', function() {
    it('should route to depoimento.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'depoimentoCtrl.index')
        ).to.have.been.calledOnce;
    });
  });

  describe('GET /api/depoimentos/:id', function() {
    it('should route to depoimento.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'depoimentoCtrl.show')
        ).to.have.been.calledOnce;
    });
  });

  describe('POST /api/depoimentos', function() {
    it('should route to depoimento.controller.create', function() {
      expect(routerStub.post
        .withArgs('/', 'depoimentoCtrl.create')
        ).to.have.been.calledOnce;
    });
  });

  describe('PUT /api/depoimentos/:id', function() {
    it('should route to depoimento.controller.upsert', function() {
      expect(routerStub.put
        .withArgs('/:id', 'depoimentoCtrl.upsert')
        ).to.have.been.calledOnce;
    });
  });

  describe('PATCH /api/depoimentos/:id', function() {
    it('should route to depoimento.controller.patch', function() {
      expect(routerStub.patch
        .withArgs('/:id', 'depoimentoCtrl.patch')
        ).to.have.been.calledOnce;
    });
  });

  describe('DELETE /api/depoimentos/:id', function() {
    it('should route to depoimento.controller.destroy', function() {
      expect(routerStub.delete
        .withArgs('/:id', 'depoimentoCtrl.destroy')
        ).to.have.been.calledOnce;
    });
  });
});
