'use strict';

import mongoose from 'mongoose';
import {registerEvents} from './depoimento.events';

var DepoimentoSchema = new mongoose.Schema({
  depoimento: {
    type: String,
    unique: true,
    required: true
  },
  emailAutor: {
    type: String,
    unique: false,
    required: true
  },
  ativo: {
    type: Boolean,
    unique: false,
    required: false,
    default: false
  }
},{ timestamps: true });

registerEvents(DepoimentoSchema);
export default mongoose.model('Depoimento', DepoimentoSchema);
