'use strict';

/* globals describe, expect, it, beforeEach, afterEach */

var app = require('../..');
import request from 'supertest';

var newDepoimento;

describe('Depoimento API:', function() {
  describe('GET /api/depoimentos', function() {
    var depoimentos;

    beforeEach(function(done) {
      request(app)
        .get('/api/depoimentos')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          depoimentos = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      expect(depoimentos).to.be.instanceOf(Array);
    });
  });

  describe('POST /api/depoimentos', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/depoimentos')
        .send({
          depoimento: 'Novo Depoimento',
          emailAutor: 'fulano@exemplo.net',
          ativo: true
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          newDepoimento = res.body;
          done();
        });
    });

    it('should respond with the newly created thing', function() {
      expect(newDepoimento.depoimento).to.equal('Novo Depoimento');
      expect(newDepoimento.ativo).to.equal(true);
    });
  });

  describe('GET /api/depoimentos/:id', function() {
    var depoimento;

    beforeEach(function(done) {
      request(app)
        .get(`/api/depoimentos/${newDepoimento._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          depoimento = res.body;
          done();
        });
    });

    afterEach(function() {
      depoimento = {};
    });

    it('should respond with the requested thing', function() {
      expect(depoimento.depoimento).to.equal('Novo Depoimento');
      expect(depoimento.ativo).to.equal(true);
    });
  });

  describe('PUT /api/depoimentos`/:id', function() {
    var updatedDepoimento;

    beforeEach(function(done) {
      request(app)
        .put(`/api/depoimentos/${newDepoimento._id}`)
        .send({
          depoimento: 'Depoimento Atualizado',
          ativo: true
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          updatedDepoimento = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedDepoimento = {};
    });

    it('should respond with the updated depoimento', function() {
      expect(updatedDepoimento.depoimento).to.equal('Depoimento Atualizado');
      expect(updatedDepoimento.ativo).to.equal(true);
    });

    it('should respond with the updated depoimento on a subsequent GET', function(done) {
      request(app)
        .get(`/api/depoimentos/${newDepoimento._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          let depoimento = res.body;

          expect(depoimento.depoimento).to.equal('Depoimento Atualizado');
          expect(depoimento.ativo).to.equal(true);

          done();
        });
    });
  });

  describe('PATCH /api/depoimentos/:id', function() {
    var patchedDepoimento;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/depoimentos/${newDepoimento._id}`)
        .send([
          { op: 'replace', path: '/depoimento', value: 'Patched Depoimento' },
          { op: 'replace', path: '/ativo', value: false }
        ])
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          patchedDepoimento = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedDepoimento = {};
    });

    it('should respond with the patched depoimento', function() {
      expect(patchedDepoimento.depoimento).to.equal('Patched Depoimento');
      expect(patchedDepoimento.ativo).to.equal(false);
    });
  });

  describe('DELETE /api/depoimentos/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/depoimentos/${newDepoimento._id}`)
        .expect(204)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when Depoimento does not exist', function(done) {
      request(app)
        .delete(`/api/depoimentos/${newDepoimento._id}`)
        .expect(404)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });
  });
});
