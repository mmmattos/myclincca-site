/**
 * Main application routes
 */

'use strict';

import errors from './components/errors';
import path from 'path';
import lusca from 'lusca';

export default function(app) {
  // Insert routes below
  app.use('/api/pings', require('./api/ping'));
  app.use('/api/medicos', require('./api/medico'));
  app.use('/auth/mobile', require('./auth/indexMobile').default);
  app.use('/api/clinicas/mobile', require('./api/clinica/indexMobile'));
  app.use('/api/agendaprocedimentos/mobile', require('./api/agendaprocedimento/indexMobile'));
  app.use('/api/procedimentos/mobile', require('./api/procedimento/indexMobile'));
  app.use('/api/tipoprocedimentos/mobile', require('./api/tipoprocedimento/indexMobile'));
  app.use('/api/users/mobile', require('./api/user/indexMobile'));
  app.use(lusca.csrf({angular: true}));
  app.use('/api/things', require('./api/thing'));
  app.use('/api/depoimentos', require('./api/depoimento'));
  app.use('/api/enlaces', require('./api/enlace'));
  app.use('/api/duvidas', require('./api/duvida'));
  app.use('/api/procedimentos', require('./api/procedimento'));
  app.use('/api/users', require('./api/user'));
  app.use('/api/agendaconsultas', require('./api/agendaconsulta'));
  app.use('/api/clinicas', require('./api/clinica'));
  app.use('/api/tipoprocedimentos', require('./api/tipoprocedimento'));
  app.use('/api/agendaprocedimentos', require('./api/agendaprocedimento'));
  app.use('/api/forgots', require('./api/forgot'));
  app.use('/auth', require('./auth').default);

  // All undefined asset or api routes should return a 404
  app.route('/:url(api|auth|components|app|bower_components|assets)/*')
   .get(errors[404]);

  // All other routes should redirect to the index.html
  app.route('/*')
    .get((req, res) => {
      res.sendFile(path.resolve(`${app.get('appPath')}/index.html`));
    });
}
