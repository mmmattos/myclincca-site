import User from './api/user/user.model';
import jwt from 'jsonwebtoken';
import config from './config/environment';

module.exports = function(req, res, next) {
  var token = (req.body && req.body.access_token) || (req.query && req.query.access_token) || req.headers['x-access-token'];
  console.log('Starting to check token...');
  console.log('Got token .......', token);
  if(token) {
    try {
      console.log('tokenSecret is...', req.app.get('tokenSecret'));
      var tokenSecret = config.secrets.session; // req.app.get('tokenSecret');
      console.log('TOKEN SECRET!!!:', tokenSecret);
      var decoded = jwt.decode(token, tokenSecret);
      console.log('DECODED!!!!:', decoded);
      console.log('DATE.NOW!!!:', Date.now());
      if(decoded.exp <= Date.now() / 1000) {
        console.log('Token expired...');
        if(req.method == 'GET' && req._parsedUrl.pathname == '/markers') {
          console.log('But based on method and pathname we can proceed.');
          User.findOne({ _id: decoded.iss }, function(err, user) {
            if(err) {
              console.log('JWT Error: ', err);
              res.end('Error handling Token Error', 401);
            }
            console.log('Found User by Token...');
            req.user = user;
            return next();
          });
          //return next();
        } else {
          console.log('Token REALLY expired...!');
          res.end('Access token has expired', 401);
        }
      } else {
        // Token still valid. Go ahead...
        User.findOne({ _id: decoded.iss }, function(err, user) {
          if(err) {
            console.log('JWT Error: ', err);
            res.end('Error handling Token Error', 401);
          }
          console.log('Found User by Token...');
          req.user = user;
          return next();
        });
      }
    } catch(err) {
      console.log('ERROR!!!!:', err.message);
      res.end('Error:', err.message);
    }
  } else {
    console.log('Token not provided...');
    // If method allows for anonymous usage, go ahead. Add all that apply, below.
    console.log(req.method);
    console.log(req._parsedUrl.pathname);
    if(req.method == 'GET' && req._parsedUrl.pathname == '/markers') {
      console.log('But based on method and pathname we can proceed.');
      return next();
    }
    res.end('Access token not provided!', 401);
  }
};
