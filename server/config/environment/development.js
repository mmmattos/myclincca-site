'use strict';
/*eslint no-process-env:0*/

// Development specific configuration
// ==================================
module.exports = {
  // MongoDB connection options - URI is the current one.
  mongo: {
    uri: 'mongodb://myclinccaAdmin:m3d1c1n4@localhost:27017/myclinccadata',
    remoteUri: 'mongodb://myclinccaAdmin:m3d1c1n4@ds129038.mlab.com:29038/myclinccadata'
  },
  // Seed database on startup
  seedDB: false,
  // Admin user email.
  adminEmail: 'myclin.cca@gmail.com'
};
