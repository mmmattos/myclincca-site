/**
* Populate DB with sample data on server start
* to disable, edit config/environment/index.js, and set `seedDB: false`
*/

'use strict';

const moment = require('moment');

import Depoimento from '../api/depoimento/depoimento.model';
import Enlace from '../api/enlace/enlace.model';
import Duvida from '../api/duvida/duvida.model';
//import Thing from '../api/thing/thing.model';
import User from '../api/user/user.model';
import Procedimento from '../api/procedimento/procedimento.model';
import TipoProcedimento from '../api/tipoprocedimento/tipoprocedimento.model';
import Clinica from '../api/clinica/clinica.model';
import AgendaProc from '../api/agendaprocedimento/agendaprocedimento.model';

// Thing.find({}).remove()
// .then(() => {
//   Thing.create({
//     name: 'Development Tools',
//     info: 'Integration with popular tools such as Webpack, Gulp, Babel, TypeScript, Karma, '
//     + 'Mocha, ESLint, Node Inspector, Livereload, Protractor, Pug, '
//     + 'Stylus, Sass, and Less.'
//   });
// });

Depoimento.find({}).remove()
.then(() => {
  Depoimento.create({
    depoimento: 'Os profissionais da MyCLin são muito atenciosos.',
    emailAutor: 'joseph.silva@terra.com',
    ativo: true
  }, {
    depoimento: 'Os profissionais da MyCLin são muito bem preparados e qualificados.',
    emailAutor: 'clemente.whitacker@terra.com.br',
    ativo: false
  }, {
    depoimento: 'Os equipamentos são da mais alta tecnologia.',
    emailAutor: 'theresa.olivier@uol.com.br',
    ativo: false
  }, {
    depoimento: 'A economia que faço indo no MyClin alivia o meu orçamento.',
    emailAutor: 'summit.faschel@gmail.com',
    ativo: true
  })
  .then(() => {
    console.log('finished populating depoimentos');
  });
});

Duvida.find({}).remove()
.then(() => {
  Duvida.create({
    duvida: 'Como marco uma consulta?',
    emailAutor: 'jose.pereira@terra.com',
    ativo: false
  }, {
    duvida: 'Como agendo um procedimento?',
    emailAutor: 'mag.costa@terra.com',
    ativo: false
  })
  .then(() => {
    console.log('finished populating duvidas');
  });
});

Enlace.find({}).remove()
.then(() => {
  Enlace.create({
    titulo: 'MyClin CCA',
    url: 'http://myclincca.com.br',
    emailAutor: 'admin@myclincca.com.br',
    ativo: true
  }, {
    titulo: 'Google',
    url: 'http://www.google.com.br/',
    emailAutor: 'felipe.maierson@uol.com.br',
    ativo: true
  })
  .then(() => {
    console.log('finished populating enlaces');
  });
});

const seedUsers = () =>
User.find({}).remove()
.then(() =>
User.create({
  provider: 'local',
  name: 'Dr. House',
  email: 'test@example.com',
  password: 'test',
  cremers: '999999',
  especialidade: 'PEDIATRIA',
  telefone: '992222222',
  rg: '2334455667',
  cpf: '34345498988',
  ativo: false
}, {
  provider: 'local',
  role: 'admin',
  name: 'Admin',
  email: 'myclin.cca@gmail.com',
  password: 'admin',
  cremers: '111111',
  especialidade: 'CLINICA',
  telefone: '992233322',
  rg: '2336665667',
  cpf: '34345496668',
  ativo: true
}))
.then(() => {
  console.log('finished populating users');
});

const seedProcedimentos = () =>
Procedimento.find({}).remove()
.then(() =>
Procedimento.create(
  { procedimento: 'Exerese até cinco lesões pequenas',
  descricao: 'Exerese até cinco lesões pequenas',
  duracao: '30',
  tipo: 'cirurgia',
  valor: 'R$220,00',
  ativo: true,
  emailAutor: 'myclin.cca@gmail.com',
  detalhes: [
    '01 avental estéril',
    '01 par de luvas estéril',
    '02 fios mononylon',
    '01 frasco de lidocaína',
    '01 campo fenestrado',
    '20 ml clorexidine',
    '01 seringa de 20ml',
    '01 seringa de 5ml',
    '01 agulha 40x12',
    '01 agulha 13x4,5',
    '02 pacotes de gase',
    '01 lâmina de bisturi 11 ou 15',
    '01 soro fisiológico de 125ml',
    '100cm micropore',
    '01 cobertura de mesa estéril'
  ]
}, {
  procedimento: 'Cantoplastia',
  descricao: 'Cantoplastia',
  valor: 'R$220,00',
  tipo: 'cirurgia',
  duracao: 30,
  ativo: true,
  emailAutor: 'myclin.cca@gmail.com',
  detalhes: [
    '01 avental estéril',
    '01 par de luvas estéril ',
    '02 fios mononylon',
    '01 frasco de lidocaína',
    '01 campo fenestrado',
    '20 ml clorexidine',
    '01 seringa de 20ml',
    '01 seringa de 5ml',
    '01 agulha 40x12',
    '01 agulha 13x4,5',
    '02 pacotes de gase',
    '01 lâmina de bisturi 11 ou 15',
    '01 soro fisiológico de 125ml',
    '100cm micropore',
    '01 cobertura de mesa estéril'
  ]
}, {
  procedimento: 'Biopsia de pele',
  descricao: 'Biopsia de pele',
  valor: 'R$220,00',
  tipo: 'cirurgia',
  duracao: 30,
  ativo: true,
  emailAutor: 'myclin.cca@gmail.com',
  detalhes: [
    '01 avental estéril',
    '01 par de luvas estéril ',
    '02 fios mononylon',
    '01 frasco de lidocaína',
    '01 campo fenestrado',
    '20 ml clorexidine',
    '01 seringa de 20ml',
    '01 seringa de 5ml',
    '01 agulha 40x12',
    '01 agulha 13x4,5',
    '02 pacotes de gase',
    '01 lâmina de bisturi 11 ou 15',
    '01 soro fisiológico de 125ml',
    '100cm micropore',
    '01 cobertura de mesa estéril'
  ]
}, {
  procedimento: 'Quelóides',
  descricao: 'Quelóides',
  valor: 'R$220,00',
  tipo: 'cirurgia',
  duracao: 30,
  ativo: true,
  emailAutor: 'myclin.cca@gmail.com',
  detalhes: [
    '01 avental estéril',
    '01 par de luvas estéril ',
    '02 fios mononylon',
    '01 frasco de lidocaína',
    '01 campo fenestrado',
    '20 ml clorexidine',
    '01 seringa de 20ml',
    '01 seringa de 5ml',
    '01 agulha 40x12',
    '01 agulha 13x4,5',
    '02 pacotes de gase',
    '01 lâmina de bisturi 11 ou 15',
    '01 soro fisiológico de 125ml',
    '100cm micropore',
    '01 cobertura de mesa estéril'
  ]
}, {
  procedimento: 'Infiltrações',
  descricao: 'Infiltrações',
  valor: 'R$220,00',
  tipo: 'cirurgia',
  duracao: 30,
  ativo: true,
  emailAutor: 'myclin.cca@gmail.com',
  detalhes: [
    '01 avental estéril',
    '01 par de luvas estéril ',
    '02 fios mononylon',
    '01 frasco de lidocaína',
    '01 campo fenestrado',
    '20 ml clorexidine',
    '01 seringa de 20ml',
    '01 seringa de 5ml',
    '01 agulha 40x12',
    '01 agulha 13x4,5',
    '02 pacotes de gase',
    '01 lâmina de bisturi 11 ou 15',
    '01 soro fisiológico de 125ml',
    '100cm micropore',
    '01 cobertura de mesa estéril'
  ]
}, {
  procedimento: 'Eletrocoagulação de lesões de pele',
  descricao: 'Eletrocoagulação de lesões de pele',
  valor: 'R$220,00',
  tipo: 'cirurgia',
  duracao: 30,
  ativo: true,
  emailAutor: 'myclin.cca@gmail.com',
  detalhes: [
    '01 avental estéril',
    '01 par de luvas estéril ',
    '02 fios mononylon',
    '01 frasco de lidocaína',
    '01 campo fenestrado',
    '20 ml clorexidine',
    '01 seringa de 20ml',
    '01 seringa de 5ml',
    '01 agulha 40x12',
    '01 agulha 13x4,5',
    '02 pacotes de gase',
    '01 lâmina de bisturi 11 ou 15',
    '01 soro fisiológico de 125ml',
    '100cm micropore',
    '01 cobertura de mesa estéril'
  ]
}, {
  procedimento: 'Correção de lóbulo de orelha',
  descricao: 'Correção de lóbulo de orelha',
  valor: 'R$220,00',
  tipo: 'cirurgia',
  duracao: 30,
  ativo: true,
  emailAutor: 'myclin.cca@gmail.com',
  detalhes: [
    '01 avental estéril',
    '01 par de luvas estéril ',
    '02 fios mononylon',
    '01 frasco de lidocaína',
    '01 campo fenestrado',
    '20 ml clorexidine',
    '01 seringa de 20ml',
    '01 seringa de 5ml',
    '01 agulha 40x12',
    '01 agulha 13x4,5',
    '02 pacotes de gase',
    '01 lâmina de bisturi 11 ou 15',
    '01 soro fisiológico de 125ml',
    '100cm micropore',
    '01 cobertura de mesa estéril'
  ]
}, {
  procedimento: 'Drenagem de abcesso',
  descricao: 'Drenagem de abcesso',
  valor: 'R$220,00',
  tipo: 'cirurgia',
  duracao: 30,
  ativo: true,
  emailAutor: 'myclin.cca@gmail.com',
  detalhes: [
    '01 avental estéril',
    '01 par de luvas estéril ',
    '02 fios mononylon',
    '01 frasco de lidocaína',
    '01 campo fenestrado',
    '20 ml clorexidine',
    '01 seringa de 20ml',
    '01 seringa de 5ml',
    '01 agulha 40x12',
    '01 agulha 13x4,5',
    '02 pacotes de gase',
    '01 lâmina de bisturi 11 ou 15',
    '01 soro fisiológico de 125ml',
    '100cm micropore',
    '01 cobertura de mesa estéril'
  ]
}, {
  procedimento: 'Lipomas',
  descricao: 'Lipomas',
  valor: 'R$220,00',
  tipo: 'cirurgia',
  duracao: 30,
  ativo: true,
  emailAutor: 'myclin.cca@gmail.com',
  detalhes: [
    '01 avental estéril',
    '01 par de luvas estéril ',
    '02 fios mononylon',
    '01 frasco de lidocaína',
    '01 campo fenestrado',
    '20 ml clorexidine',
    '01 seringa de 20ml',
    '01 seringa de 5ml',
    '01 agulha 40x12',
    '01 agulha 13x4,5',
    '02 pacotes de gase',
    '01 lâmina de bisturi 11 ou 15',
    '01 soro fisiológico de 125ml',
    '100cm micropore',
    '01 cobertura de mesa estéril'
  ]
}, {
  procedimento: 'Cistos',
  descricao: 'Cistos',
  valor: 'R$220,00',
  tipo: 'cirurgia',
  duracao: 30,
  ativo: true,
  emailAutor: 'myclin.cca@gmail.com',
  detalhes: [
    '01 avental estéril',
    '01 par de luvas estéril ',
    '02 fios mononylon',
    '01 frasco de lidocaína',
    '01 campo fenestrado',
    '20 ml clorexidine',
    '01 seringa de 20ml',
    '01 seringa de 5ml',
    '01 agulha 40x12',
    '01 agulha 13x4,5',
    '02 pacotes de gase',
    '01 lâmina de bisturi 11 ou 15',
    '01 soro fisiológico de 125ml',
    '100cm micropore',
    '01 cobertura de mesa estéril'
  ]
}, {
  procedimento: 'Correções de cicatrizes',
  descricao: 'Correções de cicatrizes',
  valor: 'R$400,00',
  tipo: 'cirurgia',
  duracao: 60,
  ativo: true,
  emailAutor: 'myclin.cca@gmail.com',
  detalhes: [
    '01 Kit cirúrgico – 01 avental, 01 cobertura mesa e 04 campos',
    '01 par de luvas estéril ',
    '20 ml clorexidine',
    '01 frasco de lidocaína',
    '01 seringa de 20ml',
    '01 seringa de 5ml',
    '01 agulha 40x12',
    '01 agulha 13x4,5',
    '03 pacotes de gase',
    '01 lâmina de bisturi 11 ou 15',
    '01 soro fisiológico de 125ml',
    '01 fio mononylon',
    '01 fio vicryl',
    '01 monocryl',
    '100cm micropore'
  ]
}, {
  procedimento: 'Hérnia umbilical',
  descricao: 'Hérnia umbilical',
  valor: 'R$450,00',
  tipo: 'cirurgia',
  duracao: 60,
  ativo: true,
  emailAutor: 'myclin.cca@gmail.com',
  detalhes: [
    '01 Kit cirúrgico – 01 avental, 01 cobertura mesa e 04 campos',
    '01 avental',
    '02 par de luvas estéril ',
    '20 ml clorexidine',
    '01 frasco de lidocaína',
    '01 seringa de 20ml',
    '01 seringa de 5ml',
    '01 agulha 40x12',
    '01 agulha 13x4,5',
    '03 pacotes de gase',
    '01 lâmina de bisturi 11 ou 15',
    '01 soro fisiológico de 125ml',
    '100cm micropore',
    '01 fio mononylon',
    '01 fio prolene ou PDS ou vicryl',
    '100cm micropore	',
    'Total sala de R$450,00'
  ]
}))
.then(() => {
  console.log('finished populating procedimentos');
});


TipoProcedimento.find({}).remove()
.then(() => {
  TipoProcedimento.create({
    tipo: 'CIRURGIAS',
    descricao: 'Pequenas cirurgias sob anestesia local, tais como: retirada de sinais de pele, cistos epidérmicos, lipomas, cantoplastias (unha encravada), pequenos tumores de pele, biópsias, alguns procedimentos ginecológicos (colocação de DIU, cauterização de lesões de colo uterino), correções de cicatriz, queloides, dentre outros.',
    emailAutor: 'myclin.cca@gmail.com',
    ativo: true
  }, {
    tipo: 'CONSULTAS',
    descricao: 'Consultas nas áreas de cirurgia geral e digestiva.',
    emailAutor: 'myclin.cca@gmail.com',
    ativo: true
  }, {
    tipo: 'AVALIAÇÕES',
    descricao: 'Destinadas às pessoas que já tem a indicação médica de algum procedimento cirúrgico e necessitam de orçamento.',
    emailAutor: 'myclin.cca@gmail.com',
    ativo: true
  })
  .then(() => {
    console.log('finished populating tipos de procedimentos');
  });
});

Clinica.find({}).remove()
.then(() => {
  Clinica.create({
    secao: '',
    tipo: 'Texto',
    ordem: 1,
    conteudo: 'Diante da necessidade de opções de fácil acesso, desburocratizada e de menor custo, nasceu a MyClin CCA com a intenção de preencher uma lacuna no atendimento de pacientes sem convênio, que necessitam de realização de procedimentos cirúrgicos de pequeno porte nas mais diferentes especialidades.',
    emailAutor: 'myclin.cca@gmail.com',
    ativo: true
  }, {
    secao: 'MyClin CCA',
    tipo: 'Texto',
    ordem: 2,
    conteudo: 'A MyClin CCA é uma clínica de centro cirúrgico ambulatorial que conta com salas cirúrgica e de recuperação. Um novo conceito para realização de procedimentos de pequeno porte ambulatoriais com as seguintes vantagens:',
    emailAutor: 'myclin.cca@gmail.com',
    ativo: true
  }, {
    secao: '',
    tipo: 'Texto',
    ordem: 3,
    conteudo:
      'Redução do tempo de espera para o procedimento;\n' +
      'Acesso desburocratizado, tanto para o paciente quanto para o médico;\n' +
      'Redução de custo para o paciente com pagamento facilitado;\n' +
      'Procedimento fora do ambiente hospitalar contribuindo na ' +
      'diminuição do risco de infecção cirúrgica.\n'
    ,
    emailAutor: 'myclin.cca@gmail.com',
    ativo: true
  }, {
    secao: 'Formas de Agendamento',
    tipo: 'Texto',
    ordem: 4,
    conteudo: 'Os procedimentos serão agendados pelos médicos após um cadastro realizado no site, aplicativo ou na própria clínica. O aplicativo será disponibilizado aos telefones celulares com sistema operacional I.O.S. e Android. O profissional médico deverá baixar o aplicativo da MyClin no seu celular, fazer login e senha, dando acesso à agenda de cirurgia, podendo escolher a data e horário e efetivar a marcação do procedimento na palma de sua mão.',
    emailAutor: 'myclin.cca@gmail.com',
    ativo: true
  }, {
    secao: 'Tipos de Procedimentos',
    tipo: 'Texto',
    ordem: 5,
    conteudo: 'A MyClin CCA oferece aos seus colaboradores local seguro e adequado para realização de procedimentos de pequeno porte com anestesia local, tais como: retirada de sinais de pele; correções de cicatrizes; cistos; lipomas; cantoplastia, dentre outros.',
    emailAutor: 'myclin.cca@gmail.com',
    ativo: true
  }, {
    secao: 'Formas de Pagamento',
    tipo: 'Texto',
    ordem: 6,
    conteudo: 'O pagamento será realizado diretamente na clínica em dinheiro ou no cartão de crédito.',
    emailAutor: 'myclin.cca@gmail.com',
    ativo: true
  }, {
    secao: 'Consultórios',
    tipo: 'Texto',
    ordem: 7,
    conteudo: 'A MyClin conta com três consultórios para atendimento médico, sendo que um deles para atendimento de pacientes com necessidades especiais. Os consultórios serão destinados à locação para os profissionais médicos que tiverem interesse. Serão disponibilizados prontuários eletrônicos, avisos de consultas e confirmação de agendamentos.',
    emailAutor: 'myclin.cca@gmail.com',
    ativo: true
  }, {
    secao: 'Sistema de Climatização',
    tipo: 'Texto',
    ordem: 8,
    conteudo: 'A MyClin CCA conta com inovação em tratamento do ar nas suas dependências. O sistema de climatização está totalmente de acordo com as legislações específicas RDC50 e ABNT NBR7256, onde foram adotadas todas as recomendações necessárias a fim de garantir a qualidade do ar adequado, reduzindo os riscos biológicos e químicos transmissíveis pelo ar.',
    emailAutor: 'myclin.cca@gmail.com',
    ativo: true
  }, {
    secao: 'Localização',
    tipo: 'Texto',
    ordem: 9,
    conteudo: 'A MyClin CCA tem localização privilegiada no Centro Histórico de Porto Alegre, próxima a grande número de consultórios, hospitais, estacionamentos, com grande facilidade de acesso de qualquer ponto da cidade, através de transporte público ou particular.',
    emailAutor: 'myclin.cca@gmail.com',
    ativo: true
  }, {
    secao: '',
    tipo: 'Imagem',
    ordem: 10,
    conteudo: 'assets/images/mapa.png',
    emailAutor: 'myclin.cca@gmail.com',
    ativo: true
  }, {
    secao: 'Contato',
    tipo: 'Texto',
    ordem: 11,
    conteudo: 'Av. Independencia, 30 Sala 10,\nCentro Histórico\nPorto Alegre - RS\n51 3024 0051\n51 99914 5647',
    emailAutor: 'myclin.cca@gmail.com',
    ativo: true
  }, {
    secao: 'Responsável Técnico',
    tipo: 'Texto',
    ordem: 12,
    conteudo: 'Dr. Márcio Fernando Lopes de Oliveira\nCREMERS 21.914',
    emailAutor: 'myclin.cca@gmail.com',
    ativo: true
  })
  .then(() => {
    console.log('finished populating seções do item CLINICA');
  });
});



const seedAgendaProcedimento = (procedimentoTitulo, datap, horariop, emailAutorp, ativop, disponivelp, namep) => {
  var procedimentoARealizar;
  var profissional;
  AgendaProc.find({}).remove()
  .then(() => {
    console.log('OLD DATA REMOVED!');
    return Procedimento.findOne({ procedimento: procedimentoTitulo }).exec()
    .then(function(proc) {
      procedimentoARealizar = proc;
      console.log('USER NAME TO LOOK FOR: ', namep);
      return User.findOne({ name: namep}).exec()
      .then(function(usr) {
        //console.log('USR:', usr);
        profissional = usr;
        return AgendaProc.create({
          data: datap,
          horario: horariop,
          procedimento: procedimentoARealizar._id,
          user: profissional._id,
          ativo: ativop,
          disponivel: disponivelp,
          emailAutor: emailAutorp
        });
        // .then(function(agendaProc) {
        //   profissional.agendados.push(agendaProc._id);
        //   profissional.save(function(saveError) {
        //     if(!saveError) {
        //       console.log('Seeded agendaprocedimento');
        //     } else {
        //       console.log('Error Saving AgendaProc: ', saveError);
        //     }
        //   });
        // });
      });
    });
  })
  .then(() => {
    console.log('finished populating Agendaprocedimento');
  });
};


const seedEmptyAgendaProcedimento = (datap, horariop, emailAutorp, ativop, disponivelp, autorizadop) => {
  return AgendaProc.create({
    data: datap,
    horario: horariop,
    ativo: ativop,
    disponivel: disponivelp,
    autorizado: autorizadop,
    emailAutor: emailAutorp,
    procedimento: null,
    user: null

  });
};

export function seedAgendaProcedimentos () {
  const horaInicial = 7.0;
  const horaFinal = 19.0;
  const dataP = moment().startOf('day')
  console.log('DATA PARAM FOR SEED: ', dataP.toString());
  AgendaProc.find({}).remove()
  .then(() => {
    console.log('OLD DATA REMOVED!');

    const timeSlots = [
      '08:00', '08:30', '09:00', '09:30',
      '10:00', '10:30', '11:00', '11:30', '12:00',
      '12:30', '13:00', '13:30', '14:00', '14:30',
      '15:00', '15:30', '16:00', '16:30', '17:00',
      '17:30'
    ];
    for (var i = 0; i<timeSlots.length; i+=1) {
      const horario = timeSlots[i];
      // BELOW JUST FOR TESTING. REMOVE!!!
      const seedNumber = Math.random();
      // const dispBool = seedNumber >= 0.5;
      const dispBool = true;
      // console.log('Horario: ', timeSlots[i], ' - Disponivel: ', dispBool);

      seedEmptyAgendaProcedimento(
        dataP.toDate() ,
        horario,
        'myclin.cca@gmail.com',
        true,
        dispBool,
        false
      );
    }
  });
};

Promise.all([
  seedUsers(),
  seedProcedimentos(),
  seedAgendaProcedimentos()
]).then(() => {
  User.count({}, (error, count) => {
    console.log('GOT USERS: ', count);
  });
  Procedimento.count({}, (error, count) => {
    console.log('GOT PROCEDIMENTOS: ', count);
  });
  AgendaProc.count({}, (error, count) => {
    if (error) {
      console.log('ERROR ON COUNT AGENDAPROCEDIMENTOS!!!: ', error);
    }
    if(count >= 0) {
      console.log('GOT AGENDAPROCEDIMENTOS: ', count);
      console.log('finished populating Agendaprocedimento');
      var today = moment().startOf('day');
      var tomorrow = moment(today).add(1, 'days');
      AgendaProc.find({data: {$gte: today.toDate(), $lt: tomorrow.toDate()}})
      .sort({horario: 1}).exec()
      .then((agenda) => {
        console.log('=======================================');
        //console.log('AGENDA: ', agenda);
        agenda.forEach((item) => {
          console.log('DATA E HORA: ', item.data.toISOString(), item.horario, ' Disponivel: ', item.disponivel);
        });
        console.log('=======================================');
      });
    }
  });
});
