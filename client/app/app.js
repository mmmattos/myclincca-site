'use strict';

import angular from 'angular';
// import ngAnimate from 'angular-animate';
import ngCookies from 'angular-cookies';
import ngResource from 'angular-resource';
import ngSanitize from 'angular-sanitize';
import 'angular-socket-io';

import uiRouter from 'angular-ui-router';
import uiBootstrap from 'angular-ui-bootstrap';
import ppDatePicker from 'angular-ui-bootstrap/src/datepicker';
// import ngMessages from 'angular-messages';
// import ngValidationMatch from 'angular-validation-match';


import {
  routeConfig
} from './app.config';

import _Auth from '../components/auth/auth.module';
import account from './account';
import admin from './admin';
import navbar from '../components/navbar/navbar.component';
import footer from '../components/footer/footer.component';
import main from './main/main.component';
import constants from './app.constants';
import util from '../components/util/util.module';
import socket from '../components/socket/socket.service';
import clinica from './clinica/clinica.component';
import depoimento from './depoimento/depoimento.component';
import enlace from './enlace/enlace.component';
import forgot from './forgot/forgot.component';
import duvida from './duvida/duvida.component';
import aplicativos from './aplicativos/aplicativos.component';
import procedimento from './procedimento/procedimento.component';
import agendaconsulta from './agendaconsulta/agendaconsulta.component';
import agendaprocedimento from './agendaprocedimento/agendaprocedimento.component';
import tipoprocedimento from './tipoprocedimento/tipoprocedimento.component';

import './app.less';

angular.module('myclinccaSiteApp', [
  ngCookies, ngResource, ngSanitize, 'btford.socket-io',
  uiRouter, uiBootstrap, ppDatePicker, _Auth, account, admin, navbar,
  footer, main, constants, socket, util, clinica, depoimento,
  aplicativos, enlace, duvida, procedimento, agendaconsulta,
  agendaprocedimento, forgot, tipoprocedimento])
  .config(routeConfig)
  .run(function($rootScope, $location, Auth) {
    'ngInject';
    // Redirect to login if route requires auth and you're not logged in

    $rootScope.$on('$stateChangeStart', function(event, next) {
      Auth.isLoggedIn(function(loggedIn) {
        if(next.authenticate && !loggedIn) {
          $location.path('/login');
        }
      });
    });
  });

angular.element(document)
  .ready(() => {
    angular.bootstrap(document, ['myclinccaSiteApp'], {
      strictDi: true
    });
  });
