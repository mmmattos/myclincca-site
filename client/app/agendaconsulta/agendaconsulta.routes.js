'use strict';

export default function($stateProvider) {
  'ngInject';
  $stateProvider
    .state('agendaconsulta', {
      url: '/agendaconsulta',
      template: '<agendaconsulta></agendaconsulta>'
    });
}
