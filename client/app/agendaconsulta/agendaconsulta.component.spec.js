'use strict';

describe('Component: AgendaconsultaComponent', function() {
  // load the controller's module
  beforeEach(module('myclinccaSiteApp.agendaconsulta'));

  var AgendaconsultaComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController) {
    AgendaconsultaComponent = $componentController('agendaconsulta', {});
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
