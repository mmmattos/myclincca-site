'use strict';
const angular = require('angular');

const uiRouter = require('angular-ui-router');

import routes from './agendaconsulta.routes';

export class AgendaconsultaComponent {
  /*@ngInject*/
  constructor() {
    this.message = 'Hello';
  }
}

export default angular.module('myclinccaSiteApp.agendaconsulta', [uiRouter])
  .config(routes)
  .component('agendaconsulta', {
    template: require('./agendaconsulta.html'),
    controller: AgendaconsultaComponent,
    controllerAs: 'agendaconsultaCtrl'
  })
  .name;
