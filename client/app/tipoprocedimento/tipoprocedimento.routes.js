'use strict';

export default function($stateProvider) {
  'ngInject';
  $stateProvider
    .state('tipoprocedimento', {
      url: '/tipoprocedimento',
      template: '<tipoprocedimento></tipoprocedimento>'
    });
}
