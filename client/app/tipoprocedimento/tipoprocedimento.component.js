'use strict';

const angular = require('angular');
const uiRouter = require('angular-ui-router');
import routes from './tipoprocedimento.routes';

export class TipoProcedimentoController {
  TipoProcedimentos = [];
  newTipoProcedimento = '';
  submitted = false;
  isLoggedIn = false;
  isAdmin = false;
  errors = {
    tipoprocedimento: undefined
  };

  /*@ngInject*/
  constructor($http, $scope, socket, Auth) {
    this.$http = $http;
    this.socket = socket;
    this.Auth = Auth;
    this.isAdmin = Auth.isAdminSync;
    this.isLoggedIn = Auth.isLoggedInSync;
    $scope.$on('$destroy', function() {
      socket.unsyncUpdates('tipoprocedimento');
    });
  }

  $onInit() {
    const $http = this.$http;
    const socket = this.socket;
    const Auth = this.Auth;
    let this2 = this;

    return $http.get('/api/tipoprocedimentos')
    .then(function(response) {
      this2.TipoProcedimentos = response.data;
      console.log('ENCONTREI!!!: ', this2.TipoProcedimentos.length);
      return Auth.isAdmin()
      .then(function(ehAdmin) {
        console.log('USUARIO É ADMIN? ', ehAdmin);
        if(!ehAdmin) {
          this2.TipoProcedimentos = this2.TipoProcedimentos.filter(function(item) {
            return item.ativo;
          });
        }
        console.log('TIPOS PROCEDIMENTOS:', this2.TipoProcedimentos);
        socket.syncUpdates('tipoprocedimento', this2.TipoProcedimentos);
      });
    });
  }

  addTipoProcedimento(form) {
    this.submitted = true;
    if(form.$valid) {
      if(this.newTipoProcedimento && this.newEmailAutor) {
        this.$http.post('/api/tipoprocedimentos', {
          tipoProcedimento: this.newTipoProcedimento,
          tipoProcedimentoDescricao: this.newTipoProcedimentoDescricao,
          emailAutor: this.newEmailAutor,
          ativo: false
        });
        this.newTipoProcedimento = '';
        this.newTipoProcedimentoDescricao = '';
        this.newEmailAutor = '';
        this.submitted = false;
      }
    }
  }

  deleteTipoProcedimento(tipoprocedimento) {
    this.$http.delete(`/api/tipoprocedimentos/${tipoprocedimento._id}`);
  }

  toggleActive(tipoprocedimento) {
    console.log('tipo de procedimento ativo? ', tipoprocedimento.ativo);
    console.log('Tipo de Procedimento ID: ', tipoprocedimento._id);
    console.log('Tipo de Procedimento: ', tipoprocedimento);

    this.$http.put(`/api/tipoprocedimentos/${tipoprocedimento._id}`, {
      tipo: tipoprocedimento.tipo,
      descricao: tipoprocedimento.descricao,
      emailAutor: tipoprocedimento.emailAutor,
      ativo: tipoprocedimento.ativo
    })
    .then(() => {
      this.newTipoProcedimento = '';
      this.newTipoProcedimentoDescricao = '';
      this.newEmailAutor = '';
      this.newTipoProcedimentoAtivo = false;
    })
    .catch(err => {
      this.errors.tipoprocedimento = err.message;
    });
  }


  limparDescricao(tipoprocedimento) {
    alert('LIMPAR');
    tipoprocedimento.descricao = '';
  }

  updateTipoProcedimento(tipoprocedimento) {
    console.log('EDITAR');
    console.log('TIPO PROCEDIMENTO DESCRICAO: ', tipoprocedimento.descricao);
    console.log('TIPO PROCEDIMENTO ID: ', tipoprocedimento._id);

    this.$http.put(`/api/tipoprocedimentos/${tipoprocedimento._id}`, {
      tipo: tipoprocedimento.tipo,
      descricao: tipoprocedimento.descricao,
      emailAutor: tipoprocedimento.emailAutor,
      ativo: tipoprocedimento.ativo
    })
    .then(() => {
      console.log('Tipo Procedimento atualizado com sucesso!');
    })
    .catch(err => {
      this.errors.tipoprocedimento = err.message;
    });
  }

}

export default angular.module('myclinccaSiteApp.tipoprocedimento', [uiRouter])
  .config(routes)
  .component('tipoprocedimento', {
    template: require('./tipoprocedimento.html'),
    controller: TipoProcedimentoController,
    controllerAs: 'vm'
  })
  .name;
