'use strict';

describe('Component: TipoprocedimentoComponent', function() {
  // load the controller's module
  beforeEach(module('myclinccaSiteApp.tipoprocedimento'));

  var TipoprocedimentoComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController) {
    TipoprocedimentoComponent = $componentController('tipoprocedimento', {});
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
