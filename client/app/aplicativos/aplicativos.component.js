'use strict';

const angular = require('angular');
const uiRouter = require('angular-ui-router');
import routes from './aplicativos.routes';

/*@ngInject*/
export class AplicativosController {
  // this.$http = $http;
  // this.socket = socket;
  //
  // $scope.$on('$destroy', function() {
  //   socket.unsyncUpdates('clinica');
  // });
}
//this.message = 'Hello';

// $onInit() {
  // this.$http.get('/api/clinica')
  //   .then(response => {
  //     this.Depoimentos = response.data;
  //     this.socket.syncUpdates('depoimento', this.Depoimentos);
  //   });
// }

export default angular.module('myclinccaSiteApp.aplicativos', [uiRouter])
  .config(routes)
  .component('aplicativos', {
    template: require('./aplicativos.html')
    //controller: ClinicaController
    //,controllerAs: 'clinicaCtrl'
  })
  .name;
