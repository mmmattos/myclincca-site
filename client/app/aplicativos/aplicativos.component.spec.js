'use strict';

describe('Component: AplicativosComponent', function() {
  // load the controller's module
  beforeEach(module('myclinccaSiteApp.aplicativos'));

  var AplicativosComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController) {
    AplicativosComponent = $componentController('aplicativos', {});
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
