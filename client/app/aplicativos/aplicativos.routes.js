'use strict';

export default function($stateProvider) {
  'ngInject';
  $stateProvider
    .state('aplicativos', {
      url: '/aplicativos',
      template: '<aplicativos></aplicativos>'
    });
}
