'use strict';

describe('Component: ClinicaComponent', function() {
  // load the controller's module
  beforeEach(module('myclinccaSiteApp.clinica'));

  var ClinicaComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController) {
    ClinicaComponent = $componentController('clinica', {});
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
