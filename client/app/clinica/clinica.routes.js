'use strict';

export default function routes($stateProvider) {
  'ngInject';
  $stateProvider.state('clinica', {
    url: '/clinica',
    template: '<clinica></clinica>'
  });
}
