'use strict';

const angular = require('angular');
const uiRouter = require('angular-ui-router');
import routes from './clinica.routes';

/*@ngInject*/
export class ClinicaController {
  Clinicas = [];
  newSecaoClinica = '';
  newConteudoSecaoClinica = '';
  submitted = false;
  isAdmin = false;
  isLoggedIn = false;
  currentUser = undefined;
  errors = {
    clinica: undefined
  };

  /*@ngInject*/
  constructor($http, $scope, socket, Auth) {
    this.$http = $http;
    this.socket = socket;
    this.Auth = Auth;
    this.isAdmin = Auth.isAdminSync;
    this.isLoggedIn = Auth.isLoggedInSync;
    this.currentUser = Auth.getCurrentUserSync;
    $scope.$on('$destroy', function() {
      socket.unsyncUpdates('clinica');
    });
  }

  $onInit() {
    const $http = this.$http;
    const socket = this.socket;
    const Auth = this.Auth;
    let este = this;
    console.log('INITIALIZING CLINICAS!!!!!!!');
    return $http.get('/api/clinicas')
      .then(response => {
        console.log('RESPONSE.DATA: ', response.data);
        este.Clinicas = response.data;
        return Auth.isAdmin()
          .then(function(ehAdmin) {
            console.log('CLINICAS ', este.Clinicas);
            if(!ehAdmin) {
              este.Clinicas = este.Clinicas.filter(function(item) {
                return item.ativo;
              });
              console.log('FILTERED CLINICAS: ',este.Clinicas.length);
            }
            socket.syncUpdates('clinica', este.Clinicas);
          });
      });
  }

  isActiveAndCanEdit(clinica) {
    return clinica.ativo && this.isAdmin;
  }


  addClinica(form) {
    const Auth = this.Auth;
    this.submitted = true;
    console.log('SUBMITTED ADD!!!!');
    if(form.$valid) {
      console.log('FORM VALID!!!!!');
      if(this.newSecaoClinica && this.newConteudoSecaoClinica) {
        this.$http.post('/api/clinicas', {
          secao: this.newSecaoClinica,
          tipo: 'Texto',
          conteudo: this.newConteudoSecaoClinica,
          ordem: 99,
          emailAutor: this.currentUser.email || 'myclin.cca@gmail.com',
          ativo: true
        });
        this.newSecaoClinica = '';
        this.newConteudoSecaoClinica = '';
        this.submitted = false;
      }
    }
  }

  limpar(clinica) {
    clinica.secao = '';
    clinica.conteudo = '';
    clinica.ativo = false;
  }

  deleteClinica(clinica) {
    this.$http.delete(`/api/clinicas/${clinica._id}`);
  }

  toggleActive(clinica) {
    console.log('Clinica ativo? ', clinica.ativo);
    console.log('CLinica ID: ', clinica._id);
    console.log('Clinica: ', clinica);

    this.$http.put(`/api/clinicas/${clinica._id}`, {
      secao: clinica.secao,
      tipo: clinica.tipo,
      conteudo: clinica.conteudo,
      ordem: clinica.ordem,
      emailAutor: clinica.emailAutor,
      ativo: clinica.ativo
    })
      .then(() => {
        console.log('Clinica successfully activated!');
      })
      .catch(err => {
        this.errors.clinica = err.message;
      });
  }

  updateClinica(clinica) {
    console.log('EDITAR');
    console.log('CLINICA SECAO: ', clinica.secao);
    console.log('CLINICA CONTEUDO: ',clinica.conteudo);
    console.log('CLINICA ID: ', clinica._id);

    this.$http.put(`/api/clinicas/${clinica._id}`, {
      tipo: clinica.tipo,
      secao: clinica.secao,
      conteudo: clinica.conteudo,
      ordem: clinica.ordem,
      emailAutor: clinica.emailAutor,
      ativo: clinica.ativo
    })
    .then(() => {
      console.log('Seção da Clínica atualizada com sucesso!');
    })
    .catch(err => {
      this.errors.tipoprocedimento = err.message;
    });
  }
}

export default angular.module('myclinccaSiteApp.clinica', [uiRouter])
  .config(routes)
  .component('clinica', {
    template: require('./clinica.html'),
    controller: ClinicaController,
    controllerAs: 'vm'
  })
  .name;
