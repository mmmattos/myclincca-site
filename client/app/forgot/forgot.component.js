'use strict';
const angular = require('angular');
const uiRouter = require('angular-ui-router');
import routes from './forgot.routes';
//
export class ForgotController {
  email = '';
  submitted = false;
  errors = {
    forgot: undefined
  };


  /*@ngInject*/
  constructor($http, $scope, socket) {
    this.$http = $http;
    this.socket = socket;
    $scope.$on('$destroy', function() {
      socket.unsyncUpdates('forgot');
    });
  }

//   $onInit() {
//     const $http = this.$http;
//     const socket = this.socket;
//     //const Auth = this.Auth;
//     let este = this;
//   }

  forgotPassword(form) {
    this.submitted = true;
    console.log('GOT INTO FORGOT COMPONENT!!!!');
    if(form.$valid) {
      if(this.email) {
        this.$http.post('/api/forgots', {
          email: this.email,
          data: Date.now()
        });
        this.email = '';
        this.submitted = false;
      }
    }
  }
}

export default angular.module('myclinccaSiteApp.forgot', [uiRouter])
  .config(routes)
  .component('forgot', {
    template: require('./forgot.html'),
    controller: ForgotController,
    controllerAs: 'vm'
  })
  .name;
