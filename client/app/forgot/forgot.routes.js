'use strict';

export default function($stateProvider) {
  'ngInject';
  $stateProvider
    .state('forgot', {
      url: '/forgot',
      template: '<forgot></forgot>'
    });
}
