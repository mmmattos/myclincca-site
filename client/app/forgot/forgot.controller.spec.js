'use strict';

describe('Controller: ForgotCtrl', function() {
  // load the controller's module
  beforeEach(module('myclinccaSiteApp.forgot'));

  var ForgotCtrl;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($controller) {
    ForgotCtrl = $controller('ForgotCtrl', {});
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
