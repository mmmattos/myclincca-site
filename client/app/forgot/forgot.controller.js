'use strict';
const angular = require('angular');

/*@ngInject*/
export function forgotController() {
  this.message = 'Hello';
}

export default angular.module('myclinccaSiteApp.forgot', [])
  .controller('ForgotController', forgotController)
  .name;
