'use strict';

export default class SettingsController {
  user = {
    oldPassword: '',
    newPassword: '',
    confirmPassword: ''
  };
  errors = {
    other: undefined
  };
  message = '';
  submitted = false;


  /*@ngInject*/
  constructor(Auth) {
    this.Auth = Auth;
  }

  changePassword(form) {
    this.submitted = true;

    if(form.$valid) {
      this.Auth.changePassword(this.user.oldPassword, this.user.newPassword)
        .then(() => {
          console.log('Senha atualizada com sucesso!');
          this.message = 'Senha atualizada com sucesso.';
          this.user.oldPassword = '';
          this.user.newPassword = '';
          this.user.confirmPassword = '';
          form.$setPristine();
          form.$setUntouched();
          form.$setValidity();
          let controlNames = Object.keys(form).filter(key => key.indexOf('$') !== 0);
          for(let name of controlNames) {
            let control = form[name];
            control.$error = {};
          }
          this.errors = {};
          this.submitted = false;
          this.errors.other = {};
          // this.form.password.$errors = {};
          // this.form.newPassword.$errors = {};
          // this.form.confirmPassword.$errors = {};
        })
        .catch(() => {
          console.log('ERROR! ON PW CHANGE!');
          form.password.$setValidity('mongoose', false);
          this.errors.other = 'Incorrect password';
          this.message = '';
        });
    }
  }
}
