'use strict';

import angular from 'angular';

export default class SignupController {
  user = {
    name: '',
    email: '',
    password: '',
    cremers: '',
    especialidade: '',
    telefone: '',
    rg: '',
    cpf: '',
    endereco: '',
    uf: '',
    cidade: '',
    estadoCivil: ''
  };
  errors = {};
  submitted = false;


  /*@ngInject*/
  constructor(Auth, $state) {
    this.Auth = Auth;
    this.$state = $state;
  }

  register(form) {
    this.submitted = true;

    if(form.$valid) {
      console.log('GOING TO SIGNUP!!!!');
      console.log('USER: ', this.user);
      return this.Auth.createUser({
        name: this.user.name,
        email: this.user.email,
        password: this.user.password,
        cremers: this.user.cremers,
        especialidade: this.user.especialidade,
        telefone: this.user.telefone,
        rg: this.user.rg,
        cpf: this.user.cpf,
        endereco: this.user.endereco,
        uf: this.user.uf,
        cidade: this.user.cidade,
        estadoCivil: this.user.estadoCivil
      })
        .then(() => {
          // Account created, redirect to home
          this.Auth.logout();
          this.$state.go('login');
        })
        .catch(err => {
          err = err.data;
          this.errors = {};
          // Update validity of form fields that match the mongoose errors
          angular.forEach(err.errors, (error, field) => {
            form[field].$setValidity('mongoose', false);
            this.errors[field] = error.message;
          });
        });
    }
  }
}
