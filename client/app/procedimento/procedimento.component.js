'use strict';

const angular = require('angular');
const uiRouter = require('angular-ui-router');
import routes from './procedimento.routes';


export class ProcedimentoComponent {
  Procedimentos = [];
  newProcedimento = '';
  newDescricaoProcedimento = '';
  newTipoProcedimento = '';
  newDuracaoProcedimento = 0;
  newValorProcedimento = '';
  newDetalhesProcedimento = [];
  submitted = false;
  isAdmin = false;
  isLoggedIn = false;
  currentUser = undefined;
  errors = {
    procedimento: undefined
  };


  /*@ngInject*/
  constructor($http, $scope, socket, Auth) {
  this.$http = $http;
  this.socket = socket;
  this.Auth = Auth;
  this.$scope = $scope;
  this.isAdmin = Auth.isAdminSync;
  this.isLoggedIn = Auth.isLoggedInSync;
  this.currentUser = Auth.getCurrentUserSync;
  $scope.$on('$destroy', function() {
      socket.unsyncUpdates('procedimento');
    });
  }

  $onInit() {
    const $http = this.$http;
    const socket = this.socket;
    const self = this;
    $http.get('/api/procedimentos')
    .then(response => {
      self.Procedimentos = response.data;
      self.Procedimentos.map((item) => {
        item.detalhes = item.detalhes.join('\n');
      });
      socket.syncUpdates('procedimento', self.Procedimentos);
    });
  }

  addProcedimento(form) {
    const self = this;
    console.log('CURRENT USER :', self.currentUser());
    this.submitted = true;
    console.log('SUBMITTED ADD!!!');
    if(form.$valid) {
      console.log('FORM IS VALID!!!');
      const detalhes = this.newDetalhesProcedimento.split('\n');
      console.log('DET ARR: ', this.newDetalhesProcedimento);
      if(this.newProcedimento
          && this.newDescricaoProcedimento
          && this.newValorProcedimento
          && this.newDuracaoProcedimento
          && this.newTipoProcedimento
          && this.newDetalhesProcedimento
          && detalhes.length > 0
         ) {
        this.$http.post('/api/procedimentos', {
          procedimento: this.newProcedimento,
          descricao: this.newDescricaoProcedimento,
          duracao: parseInt(this.newDuracaoProcedimento),
          valor: this.newValorProcedimento,
          emailAutor: this.currentUser().email,
          tipo: this.newTipoProcedimento,
          ativo: true,
          detalhes: this.newDetalhesProcedimento
        })
        .then(() => {
          this.newProcedimento = '';
          this.newEmailAutor = '';
          this.newDescricaoProcedimento = '';
          this.newValorProcedimento = '';
          this.newDuracaoProcedimento = '';
          this.newTipoProcedimento = '';
          this.newDetalhesProcedimento = [];
        })
        .catch(err => {
          console.log('ERROR: ', err);
          this.errors.procedimento = err.message;
        });
      }
      this.submitted = false;
    }
  }

  deleteProcedimento(procedimento) {
    this.$http.delete(`/api/procedimentos/${procedimento._id}`);
  }

  toggleActive(procedimento) {
    const self = this;
    console.log('Procedimeento ativo? ', procedimento.ativo);
    console.log('Procedimento ID: ', procedimento._id);
    console.log('Procedimento: ', procedimento);

    this.$http.put(`/api/procedimentos/${procedimento._id}`, {
      procedimento: procedimento.procedimento,
      tipo: procedimento.tipo,
      descricao: procedimento.descricao,
      duracao: procedimento.duracao,
      valor: procedimento.valor,
      detalhes: procedimento.detalhes,
      emailAutor: self.currentUser.email,
      ativo: procedimento.ativo
    })
      .then(() => {
        console.log('Procedimento successfully activated!');
      })
      .catch(err => {
        this.errors.clinica = err.message;
      });
  }

  updateProcedimento(procedimento) {
    const self = this;
    console.log('EDITAR');
    console.log('PROCEDIMENTO : ', procedimento.procedimento);
    console.log('PROCEDIMENTO DESCRICAO: ',procedimento.descricao);
    console.log('PROCEDIMENTO ID: ', procedimento._id);

    const detalhesArr = procedimento.detalhes.split('\n');

    this.$http.put(`/api/procedimentos/${procedimento._id}`, {
      procedimento: procedimento.procedimento,
      tipo: procedimento.tipo,
      descricao: procedimento.descricao,
      duracao: procedimento.duracao,
      valor: procedimento.valor,
      detalhes: detalhesArr,
      emailAutor: self.currentUser.email,
      ativo: procedimento.ativo
    })
      .then(() => {
        console.log('Procedimento atualizado com sucesso!');
      })
      .catch(err => {
        this.errors.tipoprocedimento = err.message;
      });
  }

}

export default angular.module('myclinccaSiteApp.procedimento', [uiRouter])
  .config(routes)
  .component('procedimento', {
    template: require('./procedimento.html'),
    controller: ProcedimentoComponent,
    controllerAs: 'vm'
  })
  .name;
