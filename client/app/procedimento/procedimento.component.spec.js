'use strict';

describe('Component: ProcedimentoComponent', function() {
  // load the controller's module
  beforeEach(module('myclinccaSiteApp.procedimento'));

  var ProcedimentoComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController) {
    ProcedimentoComponent = $componentController('procedimento', {});
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
