'use strict';

export default function($stateProvider) {
  'ngInject';
  $stateProvider
    .state('procedimento', {
      url: '/procedimento',
      template: '<procedimento></procedimento>',
      authenticate: true
    });
}
