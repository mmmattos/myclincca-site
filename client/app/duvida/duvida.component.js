'use strict';

const angular = require('angular');
const uiRouter = require('angular-ui-router');
import routes from './duvida.routes';

export class DuvidaController {
  Duvidas = [];
  newDuvida = '';
  submitted = false;
  isLoggedIn = false;
  isAdmin = false;
  errors = {
    duvida: undefined
  };

  /*@ngInject*/
  constructor($http, $scope, socket, Auth) {
    this.$http = $http;
    this.socket = socket;
    this.Auth = Auth;
    this.isAdmin = Auth.isAdminSync;
    this.isLoggedIn = Auth.isLoggedInSync;
    $scope.$on('$destroy', function() {
      socket.unsyncUpdates('duvida');
    });
  }

  $onInit() {
    const $http = this.$http;
    const socket = this.socket;
    const Auth = this.Auth;
    let este = this;

    return $http.get('/api/duvidas')
    .then(function(response) {
      este.Duvidas = response.data;
      console.log('ENCONTREI!!!: ', este.Duvidas.length);
      return Auth.isAdmin()
      .then(function(ehAdmin) {
        console.log('USUARIO É ADMIN? ', ehAdmin);
        if(!ehAdmin) {
          este.Duvidas = este.Duvidas.filter(function(item) {
            return item.ativo;
          });
        }
        console.log('DUVIDAS:', este.Duvidas);
        socket.syncUpdates('duvida', este.Duvidas);
      });
    });
  }

  addDuvida(form) {
    this.submitted = true;
    if(form.$valid) {
      if(this.newDuvida && this.newEmailAutor) {
        this.$http.post('/api/duvidas', {
          duvida: this.newDuvida,
          emailAutor: this.newEmailAutor,
          ativo: false
        });
        this.newDuvida = '';
        this.newEmailAutor = '';
        this.submitted = false;
      }
    }
  }

  deleteDuvida(duvida) {
    this.$http.delete(`/api/duvidas/${duvida._id}`);
  }

  toggleActive(duvida) {
    console.log('Duvida ativa? ', duvida.ativo);
    console.log('Duvida ID: ', duvida._id);
    console.log('Duvida: ', duvida);

    this.$http.put(`/api/duvidas/${duvida._id}`, {
      duvida: duvida.duvida,
      resposta: duvida.resposta,
      emailAutor: duvida.emailAutor,
      ativo: duvida.ativo
    })
    .then(() => {
      this.newDuvida = '';
      this.newResposta = '';
      this.newEmailAutor = '';
      this.newAtivoEnlace = false;
    })
    .catch(err => {
      this.errors.duvida = err.message;
    });
  }


  limparResposta(duvida) {
    console.log('LIMPAR');
    duvida.resposta = '';
  }

  temResposta(duvida) {
    console.log('TEM RESPOSTA: ', (typeof duvida.resposta !== 'undefined' && duvida.resposta !== ``));
    console.log('DUVIDA RESPOSTA: ', duvida.resposta);
    return (typeof duvida.resposta !== 'undefined' && duvida.resposta !== ``);
  }

  answerDuvida(duvida) {
    console.log('RESPONDER');
    console.log('Duvida RESPOSTA: ', duvida.resposta);
    console.log('Duvida ID: ', duvida._id);

    this.$http.put(`/api/duvidas/${duvida._id}`, {
      duvida: duvida.duvida,
      resposta: duvida.resposta,
      emailAutor: duvida.emailAutor,
      ativo: duvida.ativo
    })
    .then(() => {
      duvida.resposta = '';
    })
    .catch(err => {
      this.errors.duvida = err.message;
    });
  }
}

export default angular.module('myclinccaSiteApp.duvida', [uiRouter])
.config(routes)
.component('duvida', {
  template: require('./duvida.html'),
  controller: DuvidaController,
  controllerAs: 'vm'
})
.name;
