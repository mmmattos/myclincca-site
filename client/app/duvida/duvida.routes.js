'use strict';

export default function routes($stateProvider) {
  'ngInject';
  $stateProvider.state('duvida', {
    url: '/duvida',
    template: '<duvida></duvida>'
  });
}
