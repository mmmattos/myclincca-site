'use strict';

describe('Component: DuvidaComponent', function() {
  // load the controller's module
  beforeEach(module('myclinccaSiteApp.duvida'));

  var DuvidaComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController) {
    DuvidaComponent = $componentController('duvida', {});
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
