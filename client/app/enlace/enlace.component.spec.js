'use strict';

describe('Component: EnlaceComponent', function() {
  // load the controller's module
  beforeEach(module('myclinccaSiteApp.enlace'));

  var EnlaceComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController) {
    EnlaceComponent = $componentController('enlace', {});
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
