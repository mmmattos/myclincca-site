'use strict';
/* eslint no-sync: 0 */

const angular = require('angular');
const uiRouter = require('angular-ui-router');
import routes from './enlace.routes';

export class EnlaceController {
  Enlaces = [];
  newEnlace = '';
  newTituloEnlace = '';
  emailAutor = '';
  submitted = false;
  isAdmin = false;
  isLoggedIn = false;
  errors = {
    enlace: undefined
  };

  /*@ngInject*/
  constructor($http, $scope, socket, Auth) {
    this.$http = $http;
    this.socket = socket;
    this.Auth = Auth;
    this.isLoggedIn = Auth.isLoggedInSync;
    this.isAdmin = Auth.isAdminSync;

    $scope.$on('$destroy', function() {
      socket.unsyncUpdates('enlace');
    });
  }

  $onInit() {
    const $http = this.$http;
    const socket = this.socket;
    const Auth = this.Auth;
    let este = this;

    return $http.get('/api/enlaces')
    .then(function(response) {
      este.Enlaces = response.data;
      console.log('ENCONTREI!!!: ', este.Enlaces.length);
      return Auth.isAdmin()
      .then(function(ehAdmin) {
        console.log('USUARIO É ADMIN? ', ehAdmin);
        if(!ehAdmin) {
          este.Enlaces = este.Enlaces.filter(function(item) {
            return item.ativo;
          });
        }
        console.log('ENLACES:', este.Enlaces);
        socket.syncUpdates('enlace', este.Enlaces);
      });
    });
  }

  addEnlace(form) {
    this.submitted = true;
    if(form.$valid) {
      if(this.newTituloEnlace && this.newEnlace && this.newEmailAutor) {
        this.$http.post('/api/enlaces', {
          titulo: this.newTituloEnlace,
          url: this.newEnlace,
          emailAutor: this.newEmailAutor,
          ativo: false
        })
        .then(() => {
          this.newEnlace = '';
          this.newTituloEnlace = '';
          this.newEmailAutor = '';
        })
        .catch(err => {
          this.errors.enlace = err.message;
        });
      }

      this.submitted = false;
    }
  }

  deleteEnlace(enlace) {
    this.$http.delete(`/api/enlaces/${enlace._id}`);
  }

  toggleActive(enlace) {
    console.log('Enlace ativo? ', enlace.ativo);
    console.log('Enlace ID: ', enlace._id);

    this.$http.put(`/api/enlaces/${enlace._id}`, {
      titulo: enlace.titulo,
      url: enlace.url,
      emailAutor: enlace.email,
      ativo: enlace.ativo
    })
    .then(() => {
      this.newEnlace = '';
      this.newTituloEnlace = '';
      this.newEmailAutor = '';
      this.newAtivoEnlace = false;
    })
    .catch(err => {
      this.errors.enlace = err.message;
    });
  }

}

export default angular.module('myclinccaSiteApp.enlace', [uiRouter])
.config(routes)
.component('enlace', {
  template: require('./enlace.html'),
  controller: EnlaceController,
  controllerAs: 'vm'
})
.name;
