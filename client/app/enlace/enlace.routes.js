'use strict';

export default function routes($stateProvider) {
  'ngInject';
  $stateProvider.state('enlace', {
    url: '/enlace',
    template: '<enlace></enlace>'
  });
}
