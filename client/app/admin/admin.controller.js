'use strict';

export default class AdminController {
  /*@ngInject*/


  constructor($http, User, $filter) {
    this.$http = $http;
    this.User = User;
    // Use the User $resource to fetch all users
    this.users = User.query();
    this.users.$promise.then(() => {
      this.users = $filter('filter')(this.users, { role: 'user' });
    });
  }

  delete(user) {
    console.log('USUARIO ativo? ', user.ativo);
    console.log('USUARIO ID: ', user._id);
    // user.$remove();

    const apiRoute = `/api/users/${user._id}`;
    console.log('Will use API ROUTE: ', apiRoute);
    this.$http.delete(apiRoute)
      .then(result => {
        console.log('RESULT: ', result);
        if(result.status >= 200 && result.status < 400) {
          this.users.splice(this.users.indexOf(user), 1);
          console.log('RESULT: ', result);
        }
      })
      .catch(req => console.log(`err: ${req}`));
  }

  toggleActive(user) {
    console.log('USUARIO ativo? ', user.ativo);
    console.log('USUARIO ID: ', user._id);

    const apiRoute = `/api/users/${user._id}/ativo`;
    console.log('Will use API ROUTE: ', apiRoute);
    this.$http.put(apiRoute)
      .then(result => {
        console.log('RESULT: ', result);
        if(result.status === 200) {
          console.log('RESULT: ', result);
        }
      })
      .catch(req => console.log(`err: ${req}`));
  }

  search(searchTerm, pendentesAtivacao) {
    console.log('TERM SEARCHED: ', searchTerm);
    console.log('PENDENTES: ', pendentesAtivacao);

    const apiRoute = `/api/users/search/${searchTerm}/pendentes/${pendentesAtivacao}`;
    console.log('SEARCH Will use API ROUTE: ', apiRoute);
    this.users = [];
    this.$http.get(apiRoute)
    .then(result => {
      console.log('RESULT: ', result);
      if(result.status === 200) {
        this.users = result.data;
        console.log('SEARCH ACHOU ', this.users.length);
        console.log('SEARCH FOUND USERS: ', this.users);
      }
    });
  }
}
