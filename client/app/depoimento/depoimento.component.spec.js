'use strict';

describe('Component: DepoimentoComponent', function() {
  // load the controller's module
  beforeEach(module('myclinccaSiteApp.depoimento'));

  var DepoimentoComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController) {
    DepoimentoComponent = $componentController('depoimento', {});
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
