'use strict';

export default function routes($stateProvider) {
  'ngInject';
  $stateProvider.state('depoimento', {
    url: '/depoimento',
    template: '<depoimento></depoimento>'
  });
}
