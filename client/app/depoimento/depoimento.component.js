'use strict';


const angular = require('angular');
const uiRouter = require('angular-ui-router');
import routes from './depoimento.routes';

export class DepoimentoController {
  Depoimentos = [];
  newDepoimento = '';
  newEmailAutor = '';
  submitted = false;
  isAdmin = false;
  isLoggedIn = false;
  errors = {
    depoimento: undefined
  };

  /*@ngInject*/
  constructor($http, $scope, socket, Auth) {
    this.$http = $http;
    this.socket = socket;
    this.Auth = Auth;
    this.isAdmin = Auth.isAdminSync;
    this.isLoggedIn = Auth.isLoggedInSync;
    $scope.$on('$destroy', function() {
      socket.unsyncUpdates('depoimento');
    });
  }

  $onInit() {
    console.log('GETTING DEPOIMENTOS!!!!!');
    const $http = this.$http;
    const socket = this.socket;
    const Auth = this.Auth;
    let este = this;
    return $http.get('/api/depoimentos')
    .then(response => {
      este.Depoimentos = response.data;
      return Auth.isAdmin()
      .then(function(ehAdmin) {
        console.log('USUARIO É ADMIN? ', ehAdmin);
        if(!ehAdmin) {
          este.Depoimentos = este.Depoimentos.filter(function(item) {
            return item.ativo;
          });
        }
        socket.syncUpdates('depoimento', este.Depoimentos);
      });
    });
  }

  isActiveAndCanEdit(depoimento) {
    return depoimento.ativo && this.isAdmin;
  }


  addDepoimento(form) {
    this.submitted = true;
    if(form.$valid) {
      if(this.newDepoimento && this.newEmailAutor) {
        this.$http.post('/api/depoimentos', {
          depoimento: this.newDepoimento,
          emailAutor: this.newEmailAutor,
          ativo: false
        });
        this.newDepoimento = '';
        this.newEmailAutor = '';
        this.submitted = false;
      }
    }
  }

  deleteDepoimento(depoimento) {
    this.$http.delete(`/api/depoimentos/${depoimento._id}`);
  }

  toggleActive(depoimento) {
    console.log('Depoimento ativo? ', depoimento.ativo);
    console.log('Depoimento ID: ', depoimento._id);
    console.log('Depoimento: ', depoimento);

    this.$http.put(`/api/depoimentos/${depoimento._id}`, {
      depoimento: depoimento.depoimento,
      emailAutor: depoimento.emailAutor,
      ativo: depoimento.ativo
    })
    .then(() => {
      console.log('Depoimento successfully activated!');
    })
    .catch(err => {
      this.errors.depoimento = err.message;
    });
  }
}

export default angular.module('myclinccaSiteApp.depoimento', [uiRouter])
  .config(routes)
  .component('depoimento', {
    template: require('./depoimento.html'),
    controller: DepoimentoController,
    controllerAs: 'vm'
  })
  .name;
