'use strict';

export default function($stateProvider) {
  'ngInject';
  $stateProvider
    .state('agendaprocedimento', {
      url: '/agendaprocedimento',
      template: '<agendaprocedimento></agendaprocedimento>',
      authenticate: true
    });
}
