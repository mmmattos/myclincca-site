'use strict';

describe('Component: AgendaprocedimentoComponent', function() {
  // load the controller's module
  beforeEach(module('myclinccaSiteApp.agendaprocedimento'));

  var AgendaprocedimentoComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController) {
    AgendaprocedimentoComponent = $componentController('agendaprocedimento', {});
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
