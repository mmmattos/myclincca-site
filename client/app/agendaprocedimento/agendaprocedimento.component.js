/* eslint-disable no-trailing-spaces */
'use strict';

const angular = require('angular');
const uiRouter = require('angular-ui-router');
const moment = require('moment');
import routes from './agendaprocedimento.routes';

export class AgendaProcedimentoController {
  // agendaProcedimentos = [];
  // newAgendaProcedimento = '';
  newEmailAutor = null;
  submitted = false;
  isLoggedIn = false;
  isAdmin = false;
  timeSlots = [];
  medicos = [];
  procedimentos = [];
  currentUser = null;
  medicoSelected = null;
  procedimentoSelected = null;
  horarioSelected = null;
  slotSelected = null;
  pacienteNome = null;
  pacienteTelefone = null;
  pacienteEmail = null;
  pacienteDataNascimento = null;
  honorariosMedicos = null;
  observacao = null;
  targetDate = null;

  errors = {
    agendaProcedimento: undefined
  };

  /*@ngInject*/
  constructor($http, $scope, socket, Auth) {
    const self = this;
    self.$http = $http;
    self.socket = socket;
    self.Auth = Auth;
    self.isAdmin = Auth.isAdminSync;
    self.isLoggedIn = Auth.isLoggedInSync;
    self.currentUser = Auth.getCurrentUserSync();
    self.$scope = $scope;
    if(self.isAdmin) {
      self.newEmailAutor = 'myclin.cca@gmail.com';
    }
//     self.today = function() {
//       $scope.dt = new Date();
//     };
//     this.today();
    $scope.$on('$destroy', function() {
      socket.unsyncUpdates('agendaprocedimento');
    });
  }

  $onInit() {
    const $http = this.$http;
    const socket = this.socket;
    const Auth = this.Auth;
    const self = this;
    const $scope = this.$scope;

//     self.datePicker = {
//       opened: false
//     };

    self.targetDate = null;
    self.dateOptions = {
      showWeeks: false,
      showButtonBar: false,
      formatYear: 'yyyy',
      startingDay: 1,
      altInputFormats: ['d!/M!/yyyy', 'dd/MM/yyyy']
    };

    self.openDatePicker = function() {
      self.datePicker.opened = false;
    };

    self.formats = ['dd, MMMM yyyy'];
    self.format = self.formats[0];

    self.uiConfig = {
      calendar: {
        firstDay: 0,
        timeFormat: 'HH:mm',
        dateFormat: 'ddmm',
        displayEventEnd: true,
      }
    };

    const locale = JSON.parse(localStorage.getItem('locale'));
    if(locale) {
      document.write('<script src="/node_modules/angular-i18n/angular-locale_pt-br.js"></script>');
    }

    self.targetDate = Date.now();
    self.gotDateChange();
    self.getMedicos();
    self.getProcedimentos();

    // self.agendaProcedimentos = [];
    // return $http.get('/api/agendaprocedimentos')
    // .then(function(response) {
    //   self.agendaProcedimentos = response.data;
    //   console.log('ENCONTREI!!!: ', self.agendaProcedimentos.length);
    //   return Auth.isAdmin()
    //   .then(function(ehAdmin) {
    //     console.log('USUARIO É ADMIN? ', ehAdmin);
    //     self.newEmailAutor = 'myclin.cca@gmail.com';
    //     self.agendaProcedimentos = self.agendaProcedimentos.filter(function(item) {
    //       return (item.disponivel === false);
    //     });
    //     console.log('AGENDAPROCEDIMENTOS:', self.agendaProcedimentos);
    //     socket.syncUpdates('agendaprocedimento', self.agendaProcedimentos);
    //   });
    // });
  }

  clearAgendaProcedimentoForm = (() => {
    const self = this;
    self.medicoSelected = null;
    self.procedimentoSelected = null;
    self.horarioSelected = null;
    self.slotSelected = null;
    self.targetDate = null;
    self.pacienteNome = null;
    self.pacienteTelefone = null;
    self.pacienteEmail = null;
    self.pacienteDataNascimento = null;
    self.honorariosMedicos = null;
    self.observacao = null;
  });

  setHorarioSelected = ((slot) => {
    const self = this;
    self.slotSelected = slot;
    self.horarioSelected = slot.horario;
    //self.$scope.horarioSelected = horario;
    console.log('NEW HORARIO: ', self.horarioSelected);
  });

  gotDateChange = (() => {
    console.log('GOT DATE CHANGE');
    const self = this;
    self.horarioSelected = null;
    // const newDate = this.targetDate;
    if(this.targetDate) {
      if(moment(this.targetDate).isoWeekday() === 6 || moment(this.targetDate).isoWeekday() === 7) {
        this.targetDate = moment(this.targetDate).add((7 - moment(this.targetDate).isoWeekday() + 1), 'days').toDate();
      }
      const dateString = moment(this.targetDate).format('YYYY-MM-DD');
      console.log('Got the new date', dateString);
      const apiRoute = `/api/agendaprocedimentos/dodia/${dateString}`;
      console.log('Will use API ROUTE: ', apiRoute);
      self.timeSlots = [];
      this.$http.get(apiRoute)
      .then(result => {
        console.log('RESULT: ', result);
        if(result.status === 200) {
          self.timeSlots = result;
          console.log('TIMESLOTS: ', self.timeSlots);
        }
      });
    }
  });


  getFormattedDate = ((aDate) => {
    return moment.parseZone(aDate).format('DD/MM/YYYY');
  });

  getMedicos = (() => {
    const self = this;
    const apiRoute = '/api/medicos';
    console.log('Will use API ROUTE: ', apiRoute);
    self.$scope.medicos = [];
    this.$http.get(apiRoute)
    .then(result => {
      console.log('RESULT: ', result);
      if(result.status === 200) {
        self.$scope.medicos = result.data;
        console.log('ACHOU ', self.$scope.medicos.length);
        console.log('MEDICOS: ', self.$scope.medicos);
      }
    });
  });


  getProcedimentos = (() => {
    const self = this;
    const apiRoute = '/api/procedimentos/ativos';
    console.log('Will use API ROUTE: ', apiRoute);
    self.$scope.procedimentos = [];
    this.$http.get(apiRoute)
    .then(result => {
      console.log('RESULT: ', result);
      if(result.status === 200) {
        self.$scope.procedimentos = result.data;
        console.log('ACHOU ', self.$scope.procedimentos.length);
        console.log('PROCEDIMENTOS: ', self.$scope.procedimentos);
      }
    });
  });

  incompleteInfoAlert = (() => {
    const msg = 'Complete informação: Nome e Tel. Paciente, Médico e Honorários, Procedimento, data e hora.';
    alert(msg);
  });

  confirmaAgendamento(form) {
    const self = this;
    this.submitted = true;
    if (self.isAdmin() && (self.medicoSelected === null || self.procedimentoSelected === null)) {
      this.incompleteInfoAlert();
      return;
    }
    const medicoId = (self.isAdmin() ? self.medicoSelected._id : self.currentUser._id);
    console.log('ATUALIZANDO AGENDAPROCEDIMENTO!');
    console.log('SLOT TO UPDATE', self.slotSelected);
    console.log('MEDICO ID: ', medicoId);
    console.log('PROCEDIMENTO ID: ', self.procedimentoSelected._id);
    if(form.$valid) {
      console.log('GO TO UPDATE....');
      console.log('DATE: ', this.targetDate);
      console.log('JUST DATE: ', moment(this.targetDate).startOf('day'));
      console.log('HORARIO: ', this.horarioSelected);
      console.log('PACIENTE: ', this.pacienteNome);
      console.log('TELEFONE: ', this.pacienteTelefone);
      console.log('EMAIL: ', this.pacienteEmail);
      console.log('DATA NASC.: ', this.pacienteDataNascimento);
      console.log('HONOR.MEDICOS: ', this.honorariosMedicos);
      console.log('OBS.: ', this.observacao);
      if(medicoId
        && this.procedimentoSelected
        && this.targetDate
        && this.horarioSelected
        && this.pacienteNome
        && this.pacienteTelefone
        && this.honorariosMedicos
        // Email, DataNascimento e Observacao are optionals.
      ) {
        console.log('WILL POST...!!!!!');
        console.log('BY... ', self.newEmailAutor);
        if(!this.pacienteEmail) { this.pacienteEmail = ''; }
        if(!this.pacienteDataNascimento) { this.pacienteDataNascimento = ''; }
        if(!this.observacao) { this.observacao = ''; }
        this.$http.post('/api/agendaprocedimentos/confirma', {
          agendamentoId: self.slotSelected._id,
          emailAutor: 'myclin.cca@gmail.com',
          data: moment(this.targetDate).startOf('day'),
          horario: this.horarioSelected,
          ativo: true,
          disponivel: false,
          reqdSlots: this.procedimentoSelected.duracao / 30,
          pacienteNome: this.pacienteNome,
          pacienteTelefone: this.pacienteTelefone,
          pacienteEmail: this.pacienteEmail,
          pacienteDataNascimento: this.pacienteDataNascimento,
          honorariosMedicos: this.honorariosMedicos,
          observacao: this.observacao,
          user: medicoId,
          procedimento: this.procedimentoSelected._id
        })
        .then(() => {
          const savedPrevDate = this.targetDate;
          this.procedimentoSelected = null;
          this.slotSelected = null;
          this.medicoSelected = null;
          this.pacienteNome = '';
          this.pacienteTelefone = '';
          this.pacienteEmail = '';
          this.pacienteDataNascimento = '';
          this.honorariosMedicos = '';
          this.observacao = '';
          this.horarioSelected = null;
          this.targetDate = null;
          this.newEmailAutor = '';
          this.submitted = false;
          this.$onInit();
          this.targetDate = moment(savedPrevDate).startOf('day');
          this.gotDateChange();
        })
        .catch((err) => {
          if(err) {
            if(err.status === 400) {
              self.errors.agendaProcedimento = 'Não há horários continuados suficientes para este procedimento. Selecione outro hoeário e tente novamente.';
              self.horarioSelected = null;
              self.submitted = false;
              self.targetDate = moment(this.targetDate).startOf('day');
              self.gotDateChange();
            }
          }
        });
      } else {
	console.log('SOMETHING IS MISSING...!!!!');
        this.incompleteInfoAlert();
	return;
      }
    } else {
      console.log('FORM IS INVALID...!!!!');
      this.incompleteInfoAlert();
      return;
    }
  }

  // clearDatePicker(agendaProcedimento) {
  //   $scope.dt = null;
  // }

  cancelaAgendamento(agendaprocedimento) {
    const self = this;
    console.log('CANCELA AP: ', agendaprocedimento);
    this.$http.post('/api/agendaprocedimentos/cancela', {
      agendamentoId: agendaprocedimento._id,
      reqdSlots: agendaprocedimento.reqdSlots
    })
    .then((result) => {
      self.targetDate = moment(agendaprocedimento.data);
      self.gotDateChange();
      // $onInit();
      // self.timeSlots = self.timeSlots.filter(function(item) {
      //   return item._id !== agendaprocedimento._id;
      // });
      console.log('RESULT: ', result);
    })
    .catch((err) => {
      console.log(err);
    });
  }
}

export default angular.module(
  'myclinccaSiteApp.agendaprocedimento',
  [uiRouter, 'ui.bootstrap.datepicker'])
  .config(routes)
  .component('agendaprocedimento', {
    template: require('./agendaprocedimento.html'),
    controller: AgendaProcedimentoController,
    controllerAs: 'vm'
  })
  .name;
