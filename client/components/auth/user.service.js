'use strict';

export function UserResource($resource) {
  'ngInject';

  const mainUrl = '/api/users/:id/:controller';
  const params = {
    id: '@id',
    controller: '@controller',
    searchTerm: '@@searchTerm',
    pendentesAtivacao: '@pendentesAtivacao'
  };

  return $resource(mainUrl, params, {
    toggleActive: {
      method: 'PUT',
      params: {
        controller: 'ativo'
      }
    },
    changePassword: {
      method: 'PUT',
      params: {
        controller: 'password'
      }
    },
    get: {
      method: 'GET',
      params: {
        id: 'me'
      }
    },
    search: {
      method: 'GET',
      url: '/api/users/search/:searchTerm/pendentes/:pendentesAtivacao',
      isArray: true
    }
  });
}
