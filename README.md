# myclincca-site

This project was generated with the [Angular Full-Stack Generator](https://github.com/DaftMonk/generator-angular-fullstack) version 4.1.4.

## Getting Started

### Prerequisites

- [Git](https://git-scm.com/)
- [Node.js and npm](nodejs.org) Node >= 4.x.x, npm >= 2.x.x
- [Gulp](http://gulpjs.com/) (`npm install --global gulp`)
- [MongoDB](https://www.mongodb.org/) - Keep a running daemon with `mongod`

### Developing

1. Run `npm install` to install server dependencies.

2. Run `mongod` in a separate shell to keep an instance of the MongoDB Daemon running

3. Run `gulp serve` to start the development server. It should automatically open the client in your browser when ready.

## Build & development

Run `gulp build` for building and `gulp serve` for preview.

## Testing

Running `npm test` will run the unit tests with karma.



## Mongo

Cli to mongolab in development

Url:
mongodb://myclinccaAdmin:m3d1c1n4@ds129038.mlab.com:29038/myclinccadata

Cli:
mongo ds129038.mlab.com:29038/myclinccadata -u myclinccaAdmin -p m3d1c1n4

> use myclinccadata
switched to db myclinccadata
> db.createUser(
{
user: "myclinccaAdmin",
pwd: "m3d1c1n4",
roles: ["readWrite", "dbAdmin"]
}
)
Successfully added user: {
	"user" : "myclinccaAdmin",
	"roles" : [
		{
			"role" : "userAdminAnyDatabase",
			"db" : "admin"
		}
	]
}
>



SETTING THE SERVER SWAP FILE (UBUNTU 1 SO NPM DOES NOT FAIL:
# Check current swap file - This should be an empty list.
sudo swapon -s

# Check for disk space
df

# Create the swap file
sudo dd if=/dev/zero of=/swapfile bs=1024 count=1000k
sudo mkswap /swapfile
sudo swapon /swapfile

# Verify the swapfile just created.
sudo swapon -s

# Persist to reboots by adding it to fstab.
sudo nano /etc/fstab
# Add next line...
/swapfile       none    swap    sw      0       0 

# Swappiness in the file should be set to 10. Skipping this step may cause both poor performance, whereas
# setting it to 10 will cause swap to act as an emergency buffer, preventing out-of-memory crashes
echo 10 | sudo tee /proc/sys/vm/swappiness
echo vm.swappiness = 10 | sudo tee -a /etc/sysctl.conf

# To prevent the file from being world-readable, 
# set up the correct permissions on the swap file
sudo chown root:root /swapfile 
sudo chmod 0600 /swapfile


##LAUNCHING APP WITH FOREVER.
##================================
Start service with forever:

$ sudo forever list
info:    Forever processes running
data:        uid  command             script    forever pid  id logfile                        uptime
data:    [0] 5RX_ /usr/local/bin/node server.js 1801    1808    /home/miguel/.forever/5RX_.log 6:20:35:57.23

$ sudo forever stop 1808
info:    Forever stopped process:
    uid  command             script    forever pid  id logfile                        uptime
[0] 5RX_ /usr/local/bin/node server.js 1801    1808    /home/miguel/.forever/5RX_.log 6:20:36:30.780

$ sudo forever list
info:    No forever processes running

$ sudo forever start --spinSleepTime 10000 node_modules/gulp/bin/gulp.js serve
warn:    --minUptime not set. Defaulting to: 1000ms
